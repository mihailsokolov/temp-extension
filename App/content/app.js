class App {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;

        /**
         * main container element
         * @type {*}
         */
        this.$$appWrapper = this.rootScope.$$appWrapper;
        
        this._subs = [];
    }
}