class Initial {
    
    constructor() {
        
        this.postOffice = new PostOffice(false);
        
        this.$$appWrapper = $('<div class="WSP_wrapper WSP_content-scripts"></div>');
        
        $('body').prepend(this.$$appWrapper);
        
        this.postOffice.get('ui').then(ui => this.ui = ui);
        
        this.init();
    
        new Affiliate(this);
        
        
        if(window.location.hostname.indexOf('batman.dev2') !== -1) {
            const css = '.open-in-browser { display: block !important }',
                head = document.head || document.getElementsByTagName('head')[0],
                style = document.createElement('style');
    
            style.type = 'text/css';
            if (style.styleSheet){
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }
    
            head.appendChild(style);
        }

		
    }
    
    init() {
        this.showDonatingAvailable();
        //        this.showDonatingConfirmation();
    }
    
    showDonatingConfirmation() {
        new DonatingAlwaysConfirmation(this);
    }
    
    showDonatingAvailable() {
        return new DonatingAvailable(this);
    }
    
}

/**
 * Removes chrome notification, notifies the background when this tab content has been fully loaded
 */
$(document).ready(() => {
    chrome.runtime.sendMessage({event: 'contentReady'});
    // window.location.hostname.indexOf('whaleslide.com') !== -1 ? $('.chrome-notification').hide() : '';
    new Initial();
});

