class Affiliate extends App {
	constructor(rootScope) {
		super( rootScope );

		this.events();
		
		this.helpers = new ExtractDomain();
		
		this.domains = {};
		
		this.postOffice.stream('_entity').subscribe(entity => {
            if(!entity) {
		        return;
            }
            this.logo = entity.logo;
		    $('.js-charity-icon-29343').css({
                'background': `url(${this.logo})no-repeat center/cover`,
            })
            
        })
        
        
	}

	/**
	 * Every targeted SERP in every tab gets their charity hints delivered by their own affiliates content script
	 */
	events() {

		/**
		 * Fetching the links of the SERP with the search engine selector provided by the background
		 */
		this.postOffice.setEvent('setCharityHints', (selector) => {
			this.selector = selector.data[0];
			this.logo = selector.data[1];
            

			this.checkLinks();
		});

		/**
		 * Affiliate check event
		 */
		this.postOffice.setEvent('affiliateCheck', (response) => {
		    
		    $('.js-charity-icon-29343').remove();
            
			let links = response.data;
            links.forEach(link => {
                if(this.domains[link.url]) {
                    this.domains[link.url].forEach(domLink => {
                        domLink.prepend(this.icon(3));
                    })
                }
            });
		});
        
//        new MutationSummary({
//            callback: () => this.checkLinks(),
//            rootNode: $('body').context,
//            queries: [{element: "*"}]
//        });
	}
	
	checkLinks() {
	    if(!this.selector) {
	        return
        }
        $(this.selector).toArray()
                        .map(l => $(l))
                        .filter(l => this.filterProtocol(l))
                        .map(l => this.wrapLink(l));
        
        
        this.postOffice.postMessageRunTime('affiliateCheck', Object.keys(this.domains).map((d, k) => d));
    }
    
    wrapLink(l) {
	    const domain = this.helpers.extractHostname(l.attr('href'));
    
	    !this.domains[domain] ? this.domains[domain] = [] : '';
	    
        this.domains[domain].push(l);
	    
	    return l
    }
    
    filterProtocol(l) {
        return l.attr('href').indexOf('http://') !== -1 || l.attr('href').indexOf('https://') !== -1
    }

	icon(zIndex) {
		return $(`<div class='WSP_charity-icon js-charity-icon-29343' style='z-index: ${zIndex}'>&nbsp;</div>`).css({
            'background': `url(${this.logo})no-repeat center/cover`,
        })
	}
}