/**
 * READ BEFORE DEV
 * DESIGN: zpl.io/bLB5PMB
 *
 0. HTML base can take form reveal component App/ui/reveal.js. It have the same content positions
 1. For logo please use App/ui/logo/logo.js
 2. At the moment please add this popup to all pages
 3. Clicking on the Always donate show this popup zpl.io/2ZPDLWL
 4. For popup use App/ui/reveal.js component
 5. If clicked ALWAYS DONATE send event "donationDecision" to the background script with option "always"
 5. If clicked "YES, donate" send event "donationDecision" to the background script with option "yes"
 6. If clicked no thanks just hide this popup
 */


class DonatingAvailable extends App {
	constructor( rootScope ) {
		super( rootScope );

		this._subs.push(
            this.postOffice.setEvent('donationPopup', () => this.show())
        );
	}

	show() {
		this.elements();
		this.setEvents();

		this.$$appWrapper.append( this.$container );

		//logo
		this.logo = new Logo(this.rootScope, {
			size: 'medium',
			parent: this.$container.find('.js-logo'),
			title: true
		});
	}

	elements() {
	    $('.js-reveal-donating-available').remove();
        this.$container = $( this.content() );
	}

	setEvents() {
        this.$container.find('.js-donating-btn').on('click', e => {
		    const target = $(e.currentTarget);
		    const value = target.attr('data-value');
            
            switch(value) {
                case 'yes':
                    this.destroy();
                    this.postOffice.postMessageRunTime('donationDecision', value);
                    break;
                case 'no':
                    this.destroy();
                    this.postOffice.postMessageRunTime('donationDecision', value);
                    break;
                case 'always':
                    this.rootScope.showDonatingConfirmation();
                    this.destroy();
                    break;
            }
		    
			
		});
	}

	destroy() {
	    
		this.$container.remove();
        this._subs.forEach(sub => sub.unsubscribe());
	}


	content() {
		return `
        
		  <div class="WSP_reveal-donation js-reveal-donating-available" style="width: 450px;">
			
			<div class="WSP_header-small WSP_text-left">
				<div class="WSP_header-logo js-logo"></div>
			</div>

			
            <div class="WSP_details-info WSP_text-left">
                <h4 class="WSP_text-header">
                    Donation Available
                </h4>
                <p class="WSP_text-body WSP_details-info-small">
                    Purchases you make on this website will result in a donation 
                    for your chosen charity. Would you like to ensure 
                    this donation is made?
                </p>     
			</div>
			
            <div class="WSP_row WSP_details-info upset WSP_button-holder-justified WSP_display-flex">
                <div class="WSP_button js-donating-btn" data-value="yes">Yes, donate</div>
                <div class="WSP_button js-donating-btn" data-value="no">No thanks</div>
                <div class="WSP_button js-donating-btn" data-value="always">Always Donate</div>
            </div>
			
		  </div>

		`
	}
}