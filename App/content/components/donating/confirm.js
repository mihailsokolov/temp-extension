class DonatingAlwaysConfirmation extends App {
	constructor( rootScope ) {
		super( rootScope );

		this.show();
	}

	show() {
		this.elements();
		this.setEvents();
		
		this.reveal = new Reveal(this.rootScope, {
		    content: this.$container,
            logoTitle: true,
            width: '580px'
        })
	}

	elements() {
        this.$container = $( this.content() );
        
        this.confirm = new SubmitBtn({
            parent: this.$container.find('confirm-button'),
            title: 'Confirm',
            callback: () => {
                //Start monetizing
                this.postOffice.post('donationDecision', "always");
                this.confirm.state('busy')
            }
        })
	}

	setEvents() {
	    this._subs.push(
	        this.postOffice.setEvent('monetiseReady', () => {
                if(this.confirm.stateNow === 'busy') {
                    this.confirm.state('success');
                    //Close
                    setTimeout(() => {
                        this.destroy();
                        this.reveal.destroy();
                    }, 1000);
                    //Change user settings
                    this.postOffice.post('changeDonatingOption', 2)
                }
            }),
	        
	        this.postOffice.stream('_ui').subscribe(ui => {

                
            })
        );
	    
        this.$container.find('.js-cancel-btn').on('click', () => {
		    let donateAvailable = this.rootScope.showDonatingAvailable();
		    donateAvailable.show();
		    this.reveal.destroy();
		});
	}

	destroy() {
		this.$container.remove();
		this._subs.forEach(sub => sub.unsubscribe());
	}

	content() {
		return `
            <ul class="WSP_list WSP_text-left">
				<li class="WSP_cursor-pointer">
				    <div class="WSP_block" style="box-shadow: none; background: transparent; border: 0">
					    <div class="WSP_text-header">Always donate</div> 
					    
					    <p class="offset small WSP_text-default" style="line-height: 1.67; ">
                            Please confirm that you’d like to ensure all of the 
                            money you generate shopping online will be automatically 
                            donated to your chosen charity (at no cost to you).
                        </p>
                        
                        <div class="upset WSP_clearfix">
                            <div class="WSP_button js-cancel-btn WSP_float-left">Cancel</div>
                            <confirm-button class="WSP_float-right"></confirm-button>
                        </div>
					</div>
				</li>
			</ul>
		`
	}
}