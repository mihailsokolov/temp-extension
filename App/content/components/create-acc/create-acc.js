class CreateAccount extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;

        this.listeners();
        this.show();

    }

    openRecovery(data) {
    }

    listeners() {

        this.postOffice.setEvent('doSignUp', request => this.openRecovery(request.data));
    }


    show()  {
        this.$$wrapper.append(this.content);
    }



    elements() {
        this.$container = $(this.content());
        //Cancel btutton
        this.$cancel = this.$container.find('.js-recovery-cancel');
        this.$recoveryFormContainer = this.$container.find('.js-email-input')
        this.$recoverySubmit = this.$container.find('.js-submit-loader');
    }

    hide() {
        this.container.remove();

    }

    setEvents() {

    }

content()
{
    return `
        <div class="WSP_wrapper WSP_ext-create-acc">
            <div>
                <div class="WSP_header-small">
                    <span class="WSP_fab WSP_cursor-pointer WSP_with-shadow"></span>
                    <span class="WSP_details-header">Create an account </span>
                    <button class="WSP_button-main WSP_top-right-button WSP_smaller WSP_grey">Close</button>
                </div>
                <!--<div class="WSP_details-info">-->
                    <!--<p>Enter the email that is linked to your account to recieve an email with a link to reset your password. If you don’t have an email linked to your account your password canot be reset.</p>-->
                <!--</div>     -->
                <div class="WSP_details-input">
                    <input type="text" class="WSP_main-input js-email-input" placeholder="Create user name"><span class="val-icon"></span>
                    
                </div>
                <div class="WSP_details-input">
                    <input type="text" class="WSP_main-input js-email-input" placeholder="Password"><span class="val-icon"></span>
                    
                </div>
                <div class="WSP_details-input">
                    <input type="text" class="WSP_main-input js-email-input" placeholder="Repeat Password "><span class="val-icon"></span>
                    <button class="WSP_button-main WSP_bottom-right-button WSP_smaller WSP_white js-submit-loader">Submit</button>
                </div>
            </div>
        </div>
        `
}
}