class Recovery extends App {
    constructor(rootScope) {
        super(rootScope);

        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;

        this.listeners();
        this.show();

    }

    openRecovery(data) {
//        console.log("Open recovery")
    }

    listeners() {

        this.postOffice.setEvent('recovery', request => this.openRecovery(request.data));
    }


    show() {
        this.$$wrapper.append(this.content);
    }

    // close() {
    //     this.$$wrapper.detach(this.content);
    // }

    elements() {
        this.$container = $(this.content());
        //Cancel btutton
        this.$cancel = this.$container.find('.js-recovery-cancel');
        this.$recoveryFormContainer = this.$container.find('.js-email-input')
        this.$recoverySubmit = this.$container.find('.js-submit-loader');
    }

    hide() {
        this.container.remove();

    }

    setEvents() {

    }

    content() {
        return `
        <div class="WSP_header-small">
            <span class="WSP_fab WSP_cursor-pointer WSP_with-shadow"></span>
            <span class="WSP_details-header">Forgotten Details </span>
            <button class="WSP_button-main WSP_top-right-button WSP_smaller WSP_grey js-recovery-cancel">Cancel</button>
        </div>
        <div class="WSP_details-info">
            <p>Enter the email that is linked to your account to recieve an email with a link to reset your password. If you don’t have an email linked to your account your password canot be reset.</p>
        </div>     
        <div class="WSP_details-input">
            <input type="text" class="WSP_main-input js-email-input" placeholder="E-MAIL"><span class="val-icon"></span>
            <button class="WSP_button-main WSP_bottom-right-button WSP_smaller WSP_white js-submit-recovery">Submit</button>
            
        </div>
        `
    }
}