class LoginForm extends App {
    constructor(rootScope, options) {
        super(rootScope);

        this.options = {
            parent: null
        };

        this.options = Object.assign(this.options, options);

        this.show();
    }
    
    /**
     * Run list. Don't forget to destroy after use
     */
    show() {
        //Create new container
        this.elements();
        this.events();
        
        //Append list container to parent element.
        this.options.parent.append(this.$container);
    }
    
    /**
     * All needed container elements
     * @type {*}
     */
    elements() {
        this.$container = $(this.content());
        this.$form = this.$container.find(".js-login-submit-form");
        
        this.$username = this.$form.find(".js-login-username");
        this.$password = this.$form.find(".js-login-password");

        this.submit = new SubmitBtn();
        this.submit.btnTitle = 'Log in';
        this.$container.find('.js-submit').append(this.submit.get())
    }
    
    events() {
        this.$form.on('submit', e => this.onSubmit(e));
        this.$form.find('input').on('keydown', (e) => {
            this.$form.find('.WSP_error-input').removeClass('WSP_error-input');
            this.submit.state('default');
            this.emit('change', e)
        })
    }
    
    /**
     * on event. Just to register callback
     * @param event
     * @param callback
     */
    on(event, callback) {
        if(event === 'error') {
            this.errorCallback = callback;
        }
        if(event === 'success') {
            this.successCallback = callback;
        }
        if(event === 'change') {
            this.changeCallback = callback;
        }
    }
    
    emit(e, message) {
        switch(e) {
            case 'error':
                typeof this.errorCallback === 'function' ? this.errorCallback(message) : '';
                break;
            case 'success':
                typeof this.successCallback === 'function' ? this.successCallback(message) : '';
                break;
            case 'change':
                typeof this.changeCallback === 'function' ? this.changeCallback(message) : '';
                break;
        }
    }
    
    clearErrors() {
        this.$username.removeClass('WSP_error-input');
        this.$password.removeClass('WSP_error-input');
    }
    
    onSubmit(e) {
        e.preventDefault();
        
        //Clear errors before validate
        this.clearErrors();
        
        let errors = false;
        
        if(this.$username.val().trim().length < 3) {
            this.$username.addClass('WSP_error-input');
            errors = true;
        }
        if(this.$password.val().trim().length < 3) {
            this.$password.addClass('WSP_error-input');
            errors = true;
        }
        
        if(errors) {
            this.emit('error', 'Your username & password must be longer than 3 characters.');
        }
        
        if(!errors) {

            this.submit.state('busy');

            this.postOffice.post('login', {
                username:this.$username.val(), password:this.$password.val()
            }).then(
                response => {
                    this.emit('success', 'Logged in')
                    this.submit.state('success');
                },
                error => {
                    this.submit.state('error');
                    this.$username.addClass('WSP_error-input');
                    this.$password.addClass('WSP_error-input');
                    this.emit('error', 'INCORRECT EMAIL OR PASSWORD')
                }
            )
        }
        
        return false;
    }
    
    
    destroy() {
        this.$container.remove();
    }
    
    
    content() {
        return `
		<div class="js-login-form-container">
			<form class="js-login-submit-form">
				<div class="WSP_form">
					<input type="text"
						   name="username"
						   value=""
						   tabindex="1"
						   placeholder="Username"
						   class="WSP_main-input WSP_inline WSP_main-input js-login-username">
					<input type="password"
						   name="password"
						   value=""
						   tabindex="2"
						   placeholder="Password"
						   class="WSP_main-input WSP_inline password-input  WSP_main-input js-login-password">
						   
						   <hr>
				    
                    <div class="js-submit"></div>
				</div>
			</form>
		</div>`;
    }
}