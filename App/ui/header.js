class Header extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            parent: null
        };
        
        this.options = Object.assign(this.options, options);
        
        this.$container = $(this.content());
        
        /**
         * Small Login Button. Opens Login form
         * @type {*}
         */
        this.$loginBtn = this.$container.find('.js-login-btn');
        
        this.show();
    }

    show() {
        this.options.parent.append(this.$container);
    }

    hide() {
        this.$container.remove();
    }
    
    /**
     * on event. Just to register callback
     * @param event
     * @param callback
     */
    on(event, callback) {
        if(event === 'login') {
            this.errorCallback = callback;
        }
    }

    content() {
        return `
           <!-- Guest view -->
        
                <div class="WSP_header-small">
                    <span>WhileSlide </span><span class="WSP_ext-header">Extension</span>
                    <button class="WSP_button-main WSP_top-right-button WSP_smaller WSP_grey js-login-btn js-show-login">LOG IN</button>
                </div>

        `
    }
}