class UiSignUp extends App {
    constructor(rootScope) {
        super(rootScope);
    }

    show() {
        this.elements();
        this.setEvents();
        this.$$appWrapper.append(this.$container);
    }

    get() {
        this.elements();
        this.setEvents();
        return this.$container;
    }


    elements() {
        this.$container = $(this.content());
        this.$username = this.$container.find('.js-name');
        this.$password = this.$container.find('.js-password');
        this.$password2 = this.$container.find('.js-password-2');
        
        
        
        this.submitBtn = new SubmitBtn();
        this.$submitBtn = this.$container.find('.js-submit');
        this.$submitBtn.append(this.submitBtn.get())
    }
    
    setEvents() {
        this.$container.on('submit', e => this.onSubmit(e));
        
        //Unset submit button errors on key press
        this.$container.find('input').on('keydown', e => this.submitBtn.state('default'))
    }
    
    /**
     * on event. Just to register callback
     * @param event
     * @param callback
     */
    on(event, callback) {
        if(event === 'error') {
            this.errorCallback = callback;
        }
        if(event === 'success') {
            this.successCallback = callback;
        }
    }
    
    emit(e, message) {
        switch(e) {
            case 'error':
                typeof this.errorCallback === 'function' ? this.errorCallback(message) : '';
                break;
            case 'success':
                typeof this.successCallback === 'function' ? this.successCallback(message) : '';
                break;
        }
    }
    
    onSubmit(e) {
        e.preventDefault();
        
        //Clear errors before validate
        this.clearErrors();
        
        if(this.$username.val().trim().length < 3) {
            this.$username.addClass('WSP_error-input');
            this.submitBtn.state('error');
            this.emit('error', 'Your username & password must be longer than 3 characters.');
            return;
        }
        if(this.$password.val().trim().length < 3) {
            this.$password.addClass('WSP_error-input');
            this.submitBtn.state('error');
            this.emit('error', 'Your username & password must be longer than 3 characters.');
            return;
        }
        if(this.$password2.val().trim().length < 3) {
            this.$password2.addClass('WSP_error-input');
            this.submitBtn.state('error');
            this.emit('error', 'Your username & password must be longer than 3 characters.');
            return;
        }
        
        if(this.$password2.val().trim() !== this.$password.val().trim()) {
            this.$password.addClass('WSP_error-input');
            this.submitBtn.state('error');
            this.emit('error', 'YOUR PASSWORDS DON’T MATCH');
            return;
        }
    
        this.submitBtn.state('busy');
    
        this.postOffice.post('doSignUp', {
            username:this.$username.val(),
            password:this.$password.val(),
            confirmPassword:this.$password2.val()
        }).then(
            response => {
                this.submitBtn.state('success');
                this.emit('success', 'Logged in')
            },
            error => {
                this.submitBtn.state('error');
                this.emit('error', error.username[0])
            }
        )
    }
    
    clearErrors() {
        this.$container.find('.WSP_error-input').removeClass('WSP_error-input');
    }
    
    destroy() {
        this.$container.remove();
        this._subs.forEach(sub => sub.unsubscribe());
    }

    content() {
        return `
            <form>
                <div class="WSP_form">
                    <div class="WSP_details-input">
                        <input type="text" class="WSP_main-input js-name" placeholder="Create user name">
                    </div>
                    <div class="WSP_details-input WSP_pass">
                        <input type="password" class="WSP_main-input js-password" placeholder="Password">
                    </div>
        
                    <div class="WSP_details-input WSP_pass">
                        <input type="password" class="WSP_main-input js-password-2" placeholder="Repeat Password">
                    </div>
        
                    <div class="WSP_details-input WSP_clearfix">
                        <div class="WSP_float-right js-submit"></div>
                    </div>
                </div>
            </form>
        `
    }
}



