class Reveal extends App {
    constructor(rootScope, options) {
        super(rootScope);

        this.options = {
            width: 'auto',
            title: null,
            content: null,
            header: true,
            logoTitle: false,
            close: true
        };

        this.options = Object.assign(this.options, options);

        this.buttons = new Buttons();

        $(document).keyup((e) => {
            if (e.keyCode == 27) { // escape key maps to keycode `27`
                this.destroy();
            }
        });
        
        this.prevented = false;
        
        this.show();
    }

    show() {
        this.elements();
        this.events();

        this.$reveal.find("*").remove();
        this.$reveal.append(this.options.content);
        
        this.$$appWrapper.append(this.$content);
    }

    elements() {
        this.$content = $(this.content());
        this.$reveal = this.$content.find('.js-reveal-content');
        this.$revealWrapper = this.$content.find('.js-reveal-container-wrapper');
        this.$cancelBtn = this.$content.find('.js-reveal-close');

        this.logo = new Logo(this.rootScope, {
            size: 'small',
            parent: this.$content.find('.js-logo'),
            title: this.options.logoTitle
        });

    }

    events() {
        //Prevent closing reveal
        this.$reveal.on('click', e => e.stopPropagation());

        this.$content.on('click', () => this.options.close ? this.destroy() : '');
        this.$cancelBtn.on('click', () => this.destroy());

        this.$revealWrapper.on('click', e => e.stopPropagation())
    }
    
    preventDefault() {
        this.prevented = true;
        
        setTimeout(() => this.prevented = false, 100);
    }

    destroy() {
        this._emit ? this._emit('closed') : '';
    
        if(!this.prevented) {
            this.$content.remove();
            this.rootScope.errors ? this.rootScope.errors.destroy() : '';
        }
    }

    content() {
        return `
            <div class="WSP_reveal-overlay inline-vertical-middle full-width js-reveal-background">
              <div class="WSP_reveal js-reveal-container-wrapper inline-vertical-middle clear" style="width: ${this.options.width}">
                
                ${this.options.header ? `
                    <div class="WSP_header-small WSP_text-left">
                        <div class="WSP_header-logo js-logo"></div>
                        ${this.options.title ? `<div class="WSP_details-header">${this.options.title}</div>` : ''}
                    </div>
                ` : ''}
               
                ${this.options.close ? `
                    <button class="WSP_button-main WSP_reveal-close WSP_smaller WSP_grey js-reveal-close">close</button>
                `: ''}
                
                <div class="js-reveal-content"></div>
                
              </div>
            </div>`
    }
}