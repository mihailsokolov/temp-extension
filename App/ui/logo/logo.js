class Logo extends App {
    constructor(rootScope, options ) {
        super(rootScope);
        
        this.options = {
            size: 'small',
            title: true,
            titleAppend: '',
            titlePrepend: '',
            inline: true,
            parent: null,
            useDefault: false,
            useSupporting: false
        };

        this.options = Object.assign(this.options, options);


        this.timer = setTimeout(() => {
        });

        this._subs.push(
            this.postOffice.stream('_entity').subscribe(entity => {
                this.entity = entity;
                
                this.entity.title = this.entity.title || '';

                this.render();
            })
        )
    }

    render() {
        this.$container ? this.$container.remove() : '';
        
        this.data = this.entity;

        this.elements();

        if (this.options.parent) {
            this.options.parent.append(this.$container);
        }

        if (this.options.title) {
            if (this.$title) {
                this.$title.remove();
            }

//            this._title = this.options.useDefault ? this.ui.extension._default.title : this.ui.extension.defaultSites.title;
            this.$title = $(this.titleEl());

            this.options.parent.append(this.$title)
        }
    }

    elements() {
        this.$container = $(this.content());
    }

    destroy() {
        this.$container.remove();
        this._subs.forEach(sub => sub.unsubscribe())
    }

    titleEl() {
        return `
            <div class="WSP_title ${ this.options.inline ? 'WSP_inline-block' : 'upset medium WSP_large'}"
                 style="${this.options.size !== 'small' ? 'white-space: normal;' : ''}">
                ${this.options.titlePrepend || ''} 
                ${this.data.title + ` <span class="${ this.data.gradient ? 'WSP_gradient' : 'WSP_text-grey'}">${this.options.titleAppend}</span>`}
            </div>
        `
    }

    content() {
        return `
            <div class="WSP_primary-logo 
                        inline-vertical-middle 
                        ${ this.data.gradient ? 'WSP_gradient' : ''}
                        ${ this.options.size ? 'WSP_' + this.options.size : ''}">
                <div    class="WSP_primary-logo-custom inline-vertical-middle bg-cover-center ${!this.data.logo ? 'WSP_empty' : ''}" 
                        style="${this.data.logo ? `background-image: url(${this.data.logo});` : ''}"></div>
            </div>
            
        `
    }
}