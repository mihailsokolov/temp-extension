class Errors extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            parent: null,
            position: 'top'
        };
        
        this.options = Object.assign(this.options, options);

        this.timer = setTimeout(() => {});
        
        this.load()
    }

    load() {
        this.$container = $(this.content());
        this.$message = this.$container.find('.js-error-container');
    
        this.options.parent.append(this.$container);
    }

    show(
        message,
        duration = 5000
    ) {
        this.$message.text(message);
        this.$container.addClass('WSP_show');

        clearTimeout(this.timer);
        this.timer = setTimeout(() => this.destroy(), duration)
    }

    destroy() {
        this.$container.removeClass('WSP_show')
    }


    content() {
        return `
            <div class="WSP_error-container WSP_text-center ${this.options.position == 'top' ? 'WSP_top-position' : 'WSP_bottom-position'}">
                <p class="WSP_error-text WSP_text-xsmall WSP_text-bold js-error-container"></p>
            </div>
        `
    }
}