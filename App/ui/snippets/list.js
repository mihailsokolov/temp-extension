class List extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            parent:null,
            title:'',
            data:[],
            search:false,
            searchPlaceholder: 'Search organisations'
        };
        
        this.setOptions(options);
    }
    
    setOptions(options) {
        this.options = Object.assign(this.options, options);
        this.destroy();
        this.show();
    }
    
    /**
     * on event. Just to register callback
     * @param event
     * @param callback
     */
    on(event, callback) {
        if(event === 'select') {
            this.clickCallback = callback;
        }
    }
    
    unCheckAll() {
        this.options.data.forEach(data => {
            data.$el.removeClass('active');
            data.button.state('default');
        });
    }
    
    changeActive(e) {
        e.$el.addClass('active');
    }
    
    /**
     * All needed container elements
     * @type {*}
     */
    elements() {
        this.$container = $(this.content());
        this.$searchContainer = this.$container.find('.js-search-input');
        this.$searchInput = $(this.searchInput());
        this.$listContainer = this.$container.find('.js-list-container');
    }
    
    /**
     * Run list. Don't forget to destroy after use
     */
    show() {
        //Create new container
        this.elements();
        
        //If we need search append it to container
        if(this.options.search) {
            this.$searchContainer.append(this.$searchInput);
            this.addSearchEvents();
        }
        
        //Set data to the list: active, background, title, event, button
        this.options.data.forEach(data => this.setLi(data));
        
        //Append list container to parent element.
        this.options.parent.append(this.$container);
    }
    
    setLi(data) {
        data.$el = $(this.liContent(data));
        data.$el.on('click', () => {
            if(this.clickCallback) {
                this.clickCallback(data)
            } else {
                console.error('No callback provided!. Use button.on(\'select\', (e) => {e})')
            }
        });
        //Button states
        this.setLiButton(data);
        
        this.$listContainer.append(data.$el)
    }
    
    setLiButton(data) {
        //Submit button
        data.button = new SubmitBtn();
        data.button.btnTitle = 'Select';

        data.$el.find('.js-submit-btn-container').append(data.button.get());
        
        if(data.active) {
            data.button.state('success')
        }
    }
    
    /**
     * Search events
     * Filter list with data.title and request from input
     */
    addSearchEvents() {
        this.$searchInput.on('keydown, keyup', (e) => {
            const request = e.target.value.toLowerCase();
            this.options.data.forEach(async data => {
                let listText = data.title.toLowerCase();
                listText.indexOf(request) === -1 ? data.$el.hide() : data.$el.show();
            })
        });
    }
    
    /**
     * Destroy containers
     */
    destroy() {
        this.$container ? this.$container.remove() : ''
    }
    
    /**
     * Creates Organisations list in DOM
     */
    liContent(data) {
        return `
        <li class="WSP_cursor-pointer js-donate-label ${data.active ? 'active' : ''}">
            
          
                <div>
                    <span class="WSP_fab WSP_profile-fab" 
                          style="background: url(${data.logo || 'https://icnt.ch/icon?domain=' + data.url}) no-repeat center/contain; background-size: cover ">
                    </span>
                </div>
             
            <div class="js-org-name WSP_text-left"> 
                <span class="WSP_text-bold">${data.title}</span>
            </div> 
            
            <div class="js-submit-btn-container"></div>
         </li>
        `
    }
    
    /**
     * Search input element
     * @returns {string}
     */
    searchInput() {
        return `
            <li>
                <div>
                    <input type="text" class="WSP_main-input js-org-input-search" placeholder="${this.options.searchPlaceholder}">
                </div>
            </li>
        `
    }
    
    
    /**
     * Main list container
     */
    content() {
        return `
      
        <div class="WSP-list-header">
        
            ${this.options.title.length ? `<div class="WSP_list-title">${this.options.title}</div>`  : ''}
            
            
            ${this.options.search ? `
                    <div class="WSP_list-row">
                        <ul class="js-search-input"></ul>
                    </div>
            ` : ''}
            
        </div>
     

        <div class="WSP-list-container">
            <hr>
            <ul class="js-list-container"></ul>
        </div>
        `
    }
}