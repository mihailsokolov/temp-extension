class UiHelpPage extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            parent: null
        };

        this.options = Object.assign(this.options, options);
        
        this.list = [
            {
                title: 'Thank You',
                description : `
                    On behalf of WebGiv and your charity of choice,
                    we would like to say “thank you” for helping to inspire
                    change….whether for an individual, a community or
                    the world. Together we can make the world a better 
                    place.
                    
                    Also we believe every user has the right to privacy.
                    You will never be tracked and we promise to never 
                    store or sell your data.`
            },
            {
                title: 'Helpful Resources',
                description : `
                    <a href="http://webgiv.com/faq" target="_blank">Frequently Asked Question</a><br>
                    <a href="http://webgiv.com/privacy" target="_blank">Privacy Policy</a><br>
                    <a href="http://webgiv.com/terms" target="_blank">Terms and Conditions</a>
                `
            }
        ];
    
        this.show();
    }

    show() {
        this.elements();

        //Append list container to parent element.
        this.options.parent.append(this.$container);
    }

    elements() {
        this.$container = $(this.content());
        
        this.list.forEach(li => this.$container.append($(this.li(li))))
    }

    destroy() {
        this.$container.remove();
    }
    
    li(data) {
        return `
            <li class="WSP_text-left offset small"> 
                <div style="font-size: 15px; font-weight: 600;">${data.title}</div>
                <div class="upset small WSP_text-default">${data.description}</div>
            </li>
        `
    }

    content() {
        return `                          
              <ul class="WSP_list splitted"></ul>
        `
    }
}