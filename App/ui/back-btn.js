class BackBtn extends SubmitBtn {
	constructor() {
		super();
	}

	content() {
		return `<button type="reset" class="WSP_button-main WSP_smaller WSP_grey">
                    <span>Back</span>
                </button>`
	}
}