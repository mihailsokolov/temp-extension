class AboutPage extends App {
    constructor(rootScope) {
        super(rootScope);

        this.$container = $(this.content());

        this.elements();
        this.setEvents();

    }

    elements() {

        //
        // //logo
        // this.logo = new Logo(this.rootScope, {
        //     size:'medium', parent:this.$container.find('.js-logo'), title:true
        // });
        //
        // this.cancelBtn = this.container.find('.js-cancel-btn');
        //

    }

    setEvents() {

        //
        // this.questionBtn.on('click', () => {
        //     console.log('question');
        //     // this.show();
        // });



    }

    show() {
        this.$container = $(this.content());
        this.questionBtn = this.$$aboutBtn
        this.questionBtn.on('click', () => {
            this.$$appWrapper.append(this.$container);
        });
    }


    hide() {
        this.$container.detach();
    }










    content() {
        return `
           <!-- About page -->
        <div class="WSP_wrapper WSP_about-section">
            <div class="">
                <div class="WSP_header-small">
                    <span>WhileSlide </span><span class="WSP_ext-header">Extension</span>
                    <button class="WSP_button-main WSP_top-right-button WSP_smaller WSP_grey js-login-btn">LOG IN</button>
                </div>

                <div class="WSP-list-container WSP_extended-list">
                    <p class="WSP_list-title WSP_text-bold WSP_text-xsmall WSP_text-uppercase">ABOUT</p>
                    <ul>
                        <li class="WSP_cursor-pointer">
                            <div>
                                <span class="WSP_text-bold WSP_ellipsis-overflow">Default search</span>
                                <p>This is where we can explain each section of the extension so that users can fully
                                    understand what they mean. </p>
                            </div>
                        </li>
                        <li class="WSP_cursor-pointer">
                            <div>
                                <span class="WSP_text-bold WSP_ellipsis-overflow">Customise new tabs</span>
                                <p>This is where we can explain each section of the extension so that users can fully
                                    understand what they mean. They can be long or short it really doesn’t matter. So
                                    much space to chat about how awesome things are. Seriously you could write a
                                    book.</p>
                            </div>
                        </li>
                        <li class="WSP_cursor-pointer">
                            <div>
                                <span class="WSP_text-bold WSP_ellipsis-overflow">Donating</span>
                                <p>This is where we can explain each section of the extension so that users can fully
                                    understand what they mean. They can be long or short it really doesn’t matter. So
                                    much space to chat about how awesome things are. Seriously you could write a
                                    book.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            </div>
    
   
        `
    }
}