class SubmitBtn {
    constructor(options) {
        this.options = {
            title: null,
            parent: null,
            callback: null,
            grey: false
        };
        
        this.options = Object.assign(this.options, options);
        
        this.btnTitle = 'submit';
        
        if(this.options.parent) {
            this.options.parent.append(this.get())
        }
    
        this.stateNow = 'default';
    }

    get() {
        this.elements();
        this.events();

        return this.$content;
    }

    state(state) {
        this.icons.forEach(icon => icon.el.remove());
        
        this.stateNow = state;

        switch (state) {

            case 'busy':
                this.$content.append(this.getIcon(state));
                this.$content.addClass('WSP_button-icon');

                break;
            case 'success':
                this.$content.addClass('WSP_button-icon');
                this.$content.append(this.getIcon(state));
                break;
            case 'error':
                this.$content.addClass('WSP_button-icon');
                this.$content.append(this.getIcon(state));
                break;
            case 'default':
                this.$content.removeClass('WSP_button-icon');
                break;
        }
    }

    getIcon(state) {
        return this.icons.find(icon => icon.type === state).el
    }

    elements() {
        this.$content = $(this.content());

        this.icons = [
            {type: 'busy', el: $(this.loading())},
            {type: 'success', el: $(this.success())},
            {type: 'error', el: $(this.error())},
        ]
    }

    events() {
        if(this.options.callback) {
            this.$content.on('click', () => this.options.callback())
        }
    }

    destroy() {
        this.$content.remove();
    }

    loading() {
        return `
            <svg class="WSP_loader" viewBox="0 0 120 120" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <linearGradient id="gradient" x1="0.5" y1="0" x2="0.5" y2="1">
                      <stop offset="0%" stop-color="#f27fd0"/>
                      <stop offset="120%" stop-color="#3595f8"/>
                    </linearGradient>
                  </defs>
                <circle class="WSP_circle" stroke="url(#gradient)" cx="60" cy="60" r="50"></circle>
            </svg>
        `
    }

    success() {
        return `
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="17" viewBox="0 0 12 9">
                <defs>
                    <linearGradient id="a" x1="-17.422%" x2="133.281%" y1="50%" y2="50%">
                        <stop offset="0%" stop-color="#F27FD0"/>
                        <stop offset="100%" stop-color="#3595F8"/>
                    </linearGradient>
                    <path id="b" d="M17.48 21.75a.976.976 0 0 0 .117 1.41c.436.358 1.09.308 1.46-.113l3.096-3.506a.975.975 0 0 0-.056-1.356l-6.728-6.493a1.063 1.063 0 0 0-1.466 0 .976.976 0 0 0 0 1.414l6.052 5.84-2.475 2.804z"/>
                    <filter id="c" width="111.4%" height="108.3%" x="-5.7%" y="-4.2%" filterUnits="objectBoundingBox">
                        <feGaussianBlur in="SourceAlpha" result="shadowBlurInner1" stdDeviation=".5"/>
                        <feOffset in="shadowBlurInner1" result="shadowOffsetInner1"/>
                        <feComposite in="shadowOffsetInner1" in2="SourceAlpha" k2="-1" k3="1" operator="arithmetic" result="shadowInnerInner1"/>
                        <feColorMatrix in="shadowInnerInner1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0"/>
                    </filter>
                </defs>
                <g fill="none" fill-rule="nonzero" transform="rotate(90 18.5 4.899)">
                    <use fill="url(#a)" fill-rule="evenodd" xlink:href="#b"/>
                    <use fill="#000" filter="url(#c)" xlink:href="#b"/>
                </g>
            </svg>
        `
    }

    error() {
        return `
            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 12 12">
                <path fill="#EE7A7A" fill-rule="evenodd" d="M6 10.435a4.413 4.413 0 0 1-2.533-.795L9.64 3.467A4.435 4.435 0 0 1 6 10.435M1.565 6a4.435 4.435 0 0 1 6.968-3.64L2.36 8.533A4.413 4.413 0 0 1 1.565 6M6 0a6 6 0 1 0 0 12A6 6 0 0 0 6 0"/>
            </svg>
        `
    }

    content() {
        return `<button type="submit" class="WSP_button-main WSP_smaller WSP_white ${this.options.grey ? 'WSP_bg-grey' : ''}">
                    <span>${this.options.title || this.btnTitle}</span>
                </button>`
    }
}