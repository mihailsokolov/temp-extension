class ExtractDomain {

    /**
     *
     parser.protocol; // => "http:"
     parser.host;     // => "example.com:3000"
     parser.hostname; // => "example.com"
     parser.port;     // => "3000"
     parser.pathname; // => "/pathname/"
     parser.hash;     // => "#hash"
     parser.search;   // => "?search=test"
     parser.origin;   // => "http://example.com:3000"
     */


    /**
     * Get domain from link with subdomain
     * http://www.youtube.com/watch?v=ClkQA2Lb_iE ====>>>>> www.youtube.com
     */
    extractHostname(url) {
        //find & remove protocol (http, ftp, etc.) and get hostname
        let hostname = url.indexOf("://") > -1 ? url.split('/')[2] : url.split('/')[0];

        //find & remove port number && find & remove "?"
        return hostname.split(':')[0].split('?')[0];
    }

    /**
     * Get domain from link
     * http://www.youtube.com/watch?v=ClkQA2Lb_iE ====>>>>> youtube.com
     */
    extractRootDomain(url) {
        let a = document.createElement('a');
            a.href = url;
        return a.hostname.replace('www.', '');
    }

    extractRootParams(url) {
        let a = document.createElement('a');
            a.href = url;
        return a.search.replace('?', '');
    }
}