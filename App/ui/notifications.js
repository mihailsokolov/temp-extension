class Notifications extends App {
    constructor(rootScope, options) {
        super(rootScope);
    
        this.timer = setTimeout(() => {});
        
        this.options = {
            parent: null,
        };
        
        this.options = Object.assign(this.options, options);
    
        this.$container = $(this.content());
        this.$message = this.$container.find('.js-message');
        this.$title = this.$container.find('.js-title');
        this.$close = this.$container.find('.js-close');
        
        new Logo(this.rootScope, {
            parent: this.$container.find('.js-logo'),
            size: 'fit',
            title: false
        });
        
        
        this.options.parent.append(this.$container);
    }
    
    show(
        title,
        message,
        duration = 5000
    ) {
        setTimeout(() => {
            this.$message.text(message);
            this.$title.text(title);
            this.$container.addClass('WSP_show');
    
            clearTimeout(this.timer);
            this.timer = setTimeout(() => this.destroy(), duration)
        }, 100)
    }
    
    setEvents() {
        this.$close.on('click', () => this.destroy())
    }
    
    destroy() {
        this.$container.removeClass('WSP_show')
    }
    
    content() {
        return `
            <div class="WSP_notification">
                <div class="WSP_notification_preview inline-vertical-middle js-logo"></div>
                
                <div class="WSP_notification_content inline-vertical-middle">
                    <div class="WSP_notification_title js-title"></div>
                    <div class="WSP_notification_subTitle js-message"></div>
                </div>
        
                <div class="WSP_notification_button">
                    <div class="WSP_button WSP_button-active js-close">Close</div>
                </div>
            </div>
        `
    }
}