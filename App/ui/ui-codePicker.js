class UiCodePicker extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            parent: null
        };
    
        this.options = Object.assign(this.options, options);
        
        
        this.show();
        this.events = {};
    }

    show() {
        this.elements();
        this.setEvents();
        
        this.options.parent.append(this.$container);
    }

    elements() {
        this.$container = $(this.content());
        
        this.$code = this.$container.find('.js-code');
        
        //Submit
        this.submit = new SubmitBtn({
            parent: this.$container.find('ui-submit'),
            title: 'Continue'
        });
    }
    
    on(event, callback) {
        this.events[event] = callback
    }
    emit(event, message) {
        this.events[event] ? this.events[event](message) : console.error(event + ' not exist');
    }

    setEvents() {
        this.$container.find('input').on('keydown', () => {
            this.$code.removeClass('WSP_error-input');
            this.emit('changed');
            this.submit.state('default');
        });
        
        this.$container.on('submit', e => {
            e.preventDefault();
            
            if(this.submit.stateNow !== 'default') {
                return;
            }
            
            if(this.$code.val().length < 3) {
                this.$code.addClass('WSP_error-input');
                this.emit('error', 'Code must be longer than 3 characters');
                
                return;
            }
            
            this.submit.state('busy');
            
            this.postOffice.post('code', this.$code.val().trim()).then(response => {
                this.submit.state('success');
                this.emit('success', response)
            }, error => {
                this.submit.state('error');
                
                setTimeout(() => {
                    this.submit.state('default');
                }, 5000);
                
                this.emit('error', error)
            })
        })
    }

    destroy() {
        this.$container.remove();
        this.events = {};
    }

    content() {
        return `
            <form class="WSP_code-picker WSP_text-center">
                <div class=" downset medium">
                    <input type="text" class="WSP_main-input js-code" placeholder="Enter the charity code">
                </div>      
    
                <ui-submit></ui-submit>
            </form>
        `
    }
}



