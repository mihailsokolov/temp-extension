class App {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;

        /**
         * main container element
         * @type {*}
         */
        this.$$appWrapper = $('.js-wrapper');
        this.$$aboutBtn = $('.js-main-right-top-question');
        this.$$wrapper = $('.WSP_wrapper');


        this._subs = [];
    
        this.router = rootScope.router;
    }
    
    find(el) {
        return this.$container.find(el);
    }
    
    price(p) {
        let n = p.toString().split('.');
        
        if(n[1] && n[1].length === 1) {
            n[1] = n[1] + '0';
        }
        if(!n[1]) {
            n[1] = '00';
        }
        
        return n.join('.')
    }
}