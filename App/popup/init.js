/**
 * INIT extension
 * First page that, preload scripts,
 */
class Extension {
    constructor() {
        this.$$wrapper = $('.WSP_wrapper');
        /**
         * PostOffice - communication between background and content and popup scripts
         * @type {PostOffice}
         */
        this.postOffice = new PostOffice();
    
        this.preLoading = new SubmitBtn({
            parent: this.$$wrapper.find('.WSP_initial-loading')
        });
    
        this.preLoading.state('busy');

        this.run();
    }
    
    async run() {
        await this.postOffice.get('ui').then(ui => this.ui = ui);
        await this.postOffice.get('entity').then(e => this.entity = e);
        await this.postOffice.get('settings').then(s => this.settings = s);
    
        this.preLoading.destroy();
    
        this.router = new Router(this);
    
        //Help page. should be global
        new HelpPagePopup(this);
        
        //Notifications
        this.notifications = new Notifications(this, {
            parent: this.$$wrapper
        });
        
        //Global error component
        this.errors = new Errors(this, {
            parent: this.$$wrapper,
            position: 'bottom'
        });
        
        this.uiStream();
        this.entityStream();
        this.settingsStream();
        
        this.init();
    }
    
    /**
     * ROOT ui stream
     * SUBSCRIPTION
     */
    uiStream() {
        this.postOffice.stream('_ui').subscribe(ui => this.ui = ui)
    }
    
    /**
     * ROOT ui stream
     * SUBSCRIPTION
     */
    entityStream() {
        this.postOffice.stream('_entity').subscribe(entity => this.entity = entity)
    }
    
    /**
     * Settings stream
     * SUBSCRIPTION
     */
    settingsStream() {
        this.postOffice.stream('_settings').subscribe(s => this.settings = s)
    }
    
    init() {
        if(this.ui['user'] || this.settings.guestMode) {
            this.router.navigate('index')
        } else {
            
            if(!this.entity.selected) {
                this.router.navigate('firstPage');
            } else {
                this.router.navigate('authChoice');
            }
        }
    }
}

/**
 * Document ready
 */
$(document).ready(() => setTimeout(() => new Extension(), 100));