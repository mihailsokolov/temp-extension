class HelpPagePopup extends App {
    constructor(rootScope) {
        super(rootScope);
    
        this.questionButton = this.$$wrapper.find('.js-main-right-top-question');
        this.closeButton = this.$$wrapper.find('.js-main-right-top-close-question');
    
        this.questionButton.on('click', () => {
            this.questionButton.hide();
            this.closeButton.show();
            this.show();
        
            this.router.current.destroy();
        });
    
        this.closeButton.on('click', () => this.destroy());
    }

    show() {
        this.elements();
        this.setEvents();

        this.$$appWrapper.append(this.$container);
    }

    elements() {

        this.$container = $(this.content());

        /**
         * UI-Logo button
         */

        this.logo = new Logo(this.rootScope, {
            size: 'medium',
            parent: this.find('ui-logo'),
            title: true
        });


        /**
         * Help content
         */
        this.help = new UiHelpPage(this.rootScope, {
            parent: this.find('ui-help')
        });
    }

    setEvents() {
        //Close page
        this.$container.find('.js-back').on('click', () => this.destroy());
    }

    destroy() {
        this.questionButton.show();
        this.$container.remove();
        this.rootScope.init()
    }

    content() {
        return `
            <div class="WSP_help-page WSP_bg-grey">
            
                <div class="WSP_block">
                    <div class="WSP_inline-block WSP_container-logo">
                        <ui-logo></ui-logo>
                    </div>
                    
                    <div class="WSP_float-right">
                        <button class="WSP_button js-back">back</button>
                    </div>
                </div>
                
                <div class="offset little WSP_text-uppercase WSP_text-xsmall" 
                        style="padding-left: 17px; letter-spacing: 1px;">
                    About
                </div>
            
                <div class="WSP_block WSP_help-scrolling">
			        <ui-help></ui-help>
			    </div>
            </div>
        `
    }
}