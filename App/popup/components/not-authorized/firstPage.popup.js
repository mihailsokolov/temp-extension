class FirstPagePopup extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        this.guestMode = this.rootScope.settings.guestMode;
        
        this.elements();
        this.setEvents();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        this.logo = new Logo(this.rootScope, {
            size: 'large',
            parent: this.$container.find('ui-logo'),
            inline: false,
            titleAppend: 'Extension'
        });
    
        this.codePicker = new UiCodePicker(this.rootScope, {
            parent: this.find('ui-code-picker')
        })
        
    }
    
    setEvents() {
        this.codePicker.on('error', error => this.rootScope.errors.show(error.message || error));
    
        this.codePicker.on('changed', () => this.rootScope.errors.destroy());
    
        this.codePicker.on('success', () => {
            this.rootScope.errors.destroy();
            this.destroy();
            if(!this.rootScope.ui['user']) {
                this.router.navigate('authChoice')
            } else {
                this.router.navigate('index')
            }
            
        });
    
        this.find('.js-manual').on('click', () => {
            this.destroy();
            this.router.navigate('entity')
        });
        
        this.find('.js-login').on('click', () => {
            this.destroy();
            this.router.navigate('login');
        })
    }
    
    destroy() {
        this.$container.remove();
        this.rootScope.errors.destroy();
    }
    
    content() {
        return `
           <!-- Guest view -->
        <div class="WSP_login-wrapper WSP_text-center">
            <div class="WSP_content WSP_login-content">
              
                <div style="position: absolute; top: 1.1rem; right: 1rem">
				    ${ !this.rootScope.ui['user'] ? `<button class="WSP_button js-login">Log in</button>` : '' }
                </div>
            
                <div class="WSP_row WSP_header upset xlarge">
                    <ui-logo></ui-logo>
                </div>
                <div class="WSP_button-main-container downset large">
                
                    <ui-code-picker></ui-code-picker>
                    
                    <div class="WSP_text-xsmall upset" style="letter-spacing: 1px;">
                        DON’T KNOW YOUR CODE? 
                        <a class="WSP_cursor-pointer WSP_text-bold js-manual">
                            SEARCH MANUALLY
                        </a>
                    </div>
                </div>
            </div>
        </div>
        `
    }
    
}