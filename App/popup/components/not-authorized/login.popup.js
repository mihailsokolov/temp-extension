class LogInPopup extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }

    show() {
        this.elements();
        this.setEvents();
        


        this.$$appWrapper.append(this.$container);
    }

    elements() {
        this.$container = $(this.content());
    
        this.loginForm = new LoginForm(this.rootScope, {
            parent: this.find('ui-login')
        });
    
        this.logo = new Logo(this.rootScope, {
            size: 'large',
            parent: this.find('ui-logo'),
            inline: false,
            title: false
        });
    }

    setEvents() {
        this.loginForm.on('error', (message) => {
            this.rootScope.errors.show(message)
        });
        
        this.loginForm.on('success', () => {
            this.runFirstPage();
        });
        
        this.loginForm.on('change', () => this.rootScope.errors.destroy());

        //Cancel
        this.find('.js-login-cancel').on('click', () => this.runFirstPage());
    
        this.find('.js-recovery').on('click', () => this.postOffice.postMessageRunTime('triggerSplash', 'recovery'))
    }
    
    runFirstPage() {
        this.destroy();
        this.rootScope.init();
    }
    
    destroy() {
        this.$container.remove();
        this.rootScope.errors.destroy();
        this.loginForm.destroy();
    }
    
    content() {
        return `
        <!--Login wrapper-->
        <div class="WSP_login-page WSP_text-center">
            <div class="WSP_login-content upset xlarge">
                
                <div style="position: absolute; top: 1.1rem; right: 1rem">
				    <button class="WSP_button js-login-cancel">back</button>
                </div>
                
                <ui-logo></ui-logo>
                
                <div class="upset tiny">
                    <ui-login></ui-login>
                </div>
                
                <a class="js-recovery WSP_link-button WSP_inline-block WSP_text-uppercase WSP_text-xsmall">
                    FORGOTTEN DETAILS
                </a>
            </div>
        </div>
        `
    }
}