class SplashAuthChoicePage extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        this.logo = new Logo(this.rootScope, {
            size: 'large',
            parent: this.$container.find('ui-logo'),
            inline: false,
//            titleAppend: 'Extension'
        });
        
    }
    
    setEvents() {
        
        this.find('.js-login').on('click', () => {
            this.destroy();
            this.router.navigate('login');
        });
        
        this.find('.js-signup').on('click', () => this.postOffice.postMessageRunTime('triggerSplash', 'signUp'));
        
        this.find('.js-guest').on('click', () => {
            this.postOffice.post('guestMode', true).then(() => {
                this.destroy();
                this.router.navigate('index')
            });
        });
        
        this.find('.js-back').on('click', () => {
            this.destroy();
            this.router.navigate('firstPage');
        })
    }
    
    destroy() {
        this.$container.remove();
        this._subs.forEach(sub => sub.unsubscribe());
    }
    
    content() {
        return `

            <div class="WSP_login-page WSP_text-center">
                <div class="WSP_login-content upset xlarge">
                    <div style="position: absolute; top: 1.1rem; right: 1rem">
                        <button class="WSP_button js-back">Back</button>
                    </div>
                    
                    <ui-logo></ui-logo>
                    
                    <div class="upset large">
                        <button class="WSP_button-main js-signup">
                            CREATE ACCOUNT
                        </button>
                    </div>
                    
                    <div class="upset small">
                        <button class="WSP_button-main WSP_grey js-guest" style="background-color: #f1f2f4;">
                            CONTINUE AS GUEST
                        </button>
                    </div>
                    
                    <div class="WSP_text-xsmall upset" style="letter-spacing: 1px;">
                        ALREADY HAVE AN ACCOUNT? <a class="WSP_cursor-pointer WSP_text-bold js-login">LOG IN</a>
                    </div>
                    
                </div>
            </div>
        `
    }
}