class ContributionTotal extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            parent:null
        };
        
        this.options = Object.assign(this.options, options);
        
        this._subs.push(
            
            this.postOffice.stream('_ui').subscribe(ui => {
                this._ui = ui;//.settings.supporting.raised_by_you;
        
                this.guestMode = this.rootScope.settings.guestMode;
    
                //Currency symbol
                this.symbol = this.rootScope.entity.symbol;
                
                if(this.guestMode) {
                    this.show()
                } else {
    
                    
        
                    if(this.rootScope.settings.raised_by_you) {
                        
                        this.postOffice.get('revenue').then(revenue => {
                            if(revenue) {
                                this.revenue = revenue;
    
                                this.revenue.total = Math.ceil((this.revenue.total) * 100) / 100;
                                this.revenue.pending = Math.ceil((this.revenue.pending) * 100) / 100;
    
                                this.show();
                            }
    
                            
                        })
            
                    } else {
                        this.$container ? this.$container.remove() : '';
                    }
                    
                    
                    
                    
                }
            })
        
        )
    }
    
    show() {
        this.elements();
        this.setEvents();
    
        this.options.parent.find('*').remove();
        this.options.parent.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
    }
    
    setEvents() {
        this.find('.js-get-started').on('click', () => {
            this.postOffice.postMessageRunTime('triggerSplash', 'signUp');
        })
    }
    
    destroy() {
        this.$container ? this.$container.remove() : '';
        
        this._subs.forEach(sub => sub.unsubscribe());
    }
    
    content() {
        return `
           <!-- Guest view -->
            <li>
                <div class="WSP_block WSP_tiny WSP_clearfix" style="padding-right: 13px">
                    <div class="WSP_float-left">
                    
                        <div class="WSP_text-bold">Your personal contribution </div>
                        
                        <div class="WSP_text-grey WSP_text-xlsmall">
                            ${this.guestMode ? 'You need an account for this feature' : 'Pending contributions*'}
                        </div>
                    </div>
                        
                ${this.guestMode ? `
                    <div class="WSP_float-right upset micro">
                        <button class="WSP_button js-get-started">GET STARTED</button>
                    </div>
                    ` : `
                    <div class="WSP_float-right upset tiny">
                        <b>${this.symbol}${this.price(this.revenue.total)}</b> / <span class="WSP_text-grey">${this.symbol}${ this.price(this.revenue.pending) }*</span>
                    </div>
                    `
                }
                </div>
            </li>
        `
    }
    
}