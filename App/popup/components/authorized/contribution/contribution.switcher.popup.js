class ContributionSwitcher extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            parent:null
        };
        
        this.options = Object.assign(this.options, options);
        
        
        this._subs.push(
    
            this.postOffice.stream('_settings').subscribe(settings => {
        
                this.guestMode = this.rootScope.settings.guestMode;
        
                this.options.parent.find('*').remove();
        
                if(this.rootScope.ui['user']) {
                    this.show();
                    this.$switcher.attr('checked', this.rootScope.settings.raised_by_you )
                }
            })
            
        )
        
        
    }
    
    show() {
        this.elements();
        this.setEvents();
    
        
        
        this.options.parent.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        this.$switcher = this.$container.find('.js-contribution');
    }
    
    setEvents() {
        this.$switcher.on('click', () => {
            this.postOffice.post('settingsContributionOption', !this.rootScope.settings.raised_by_you)
        });
    }
    
    destroy() {
        this.$container ? this.$container.remove() : '';
        
        this._subs.forEach(sub => sub.unsubscribe());
    }
    
    content() {
        return `
           <!-- Guest view -->
           <ul class="WSP_list WSP_fixed-height upset">
                <li>
                    <div class="WSP_block WSP_tiny WSP_clearfix">
                        <div class="WSP_float-left">
                            <div class="WSP_text-bold">Your personal contribution </div>
                            <div class="WSP_text-grey WSP_text-xlsmall">See the money you have raised for your cause*</div>
                        </div>
                        <div class="WSP_float-right WSP_text-bold">
                            <label class="WSP_switch-button">
                                <input type="checkbox" class="js-contribution">
                                <span class="WSP_slider"></span>
                            </label>
                        </div>
                    </div>
                </li>
            </ul>
        `
    }
    
}