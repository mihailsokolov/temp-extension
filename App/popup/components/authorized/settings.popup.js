class SettingsPopup extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.guestMode = rootScope.settings.guestMode;
        
        
        this.donationOptions = [
            {title: 'Ask everytime', id: 1},
            {title: 'Always', id: 2},
            {title: 'Never', id: 0}
        ];
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        const option = this.rootScope.settings.donating_everywhere;
        
        this.find('.js-donation-option').text(
            this.donationOptions.find(o => o.id == option).title
        );
        
        this.logo = new Logo(this.rootScope, {
            parent: this.find('ui-logo')
        })
    }
    
    setEvents() {
    
        this.find('.js-search-options').on('click', () => {
            //            this.rootScope.showDefaultSearch();
            this.destroy();
        });
    
        this.find('.js-logout-btn').on('click', () => {
            this.postOffice.post('logout').then(ui => {
                this.destroy();
                if(this.rootScope.settings.guestMode) {
                    this.router.navigate('index');
                } else {
                    this.router.navigate('firstPage');
                }
            });
            
        });
        
        this.find('.js-back').on('click', () => {
            this.destroy();
            this.router.navigate('index');
        });
        
        this.find('.js-donate-regularity').on('click', () => {
            this.router.navigate('donating');
            this.destroy();
        });
        
        this.find('.js-login').on('click', () => {
            this.router.navigate('login');
            this.destroy();
        });
        
        this.find('.js-feedback').on('click', () => {
            this.postOffice.get('openFeedback')
        })
    }
    
    destroy() {
        this.$container.remove();
    }
    
    content() {
        return `
           <!-- Guest view -->
        <div class="WSP_bg-grey WSP_full-screen">
           
			<div class="WSP_block">
				<div class="WSP_inline-block WSP_container-logo">
					<ui-logo></ui-logo>
				</div>
				
				<div class="WSP_float-right">
				    <button class="WSP_button js-back">back</button>
                </div>
			</div>
			
			<div class="offset little WSP_text-uppercase WSP_text-xsmall" style="padding-left: 17px;  letter-spacing: 1px;">
			    Settings
            </div>
			
           
			${ Config.default_search ? `
            <ul class="WSP_list WSP_fixed-height">
				<li class="WSP_cursor-pointer js-search-options">
					<div class="WSP_block WSP_clearfix">
						<div class="WSP_float-left">
							<span class="WSP_text-bold">Default Search</span>
						</div>
						<div class="WSP_float-right">
							<span class="WSP_text-grey WSP_inline-block WSP_right-arr ">WhaleSlide</span>
						</div>
					</div>
				</li>
			</ul>
            ` : '' }
			
			
			<ul class="WSP_list WSP_fixed-height  ${Config.default_search ? 'upset large' : ''}">
				<li class="WSP_cursor-pointer js-donate-regularity">
					<div class="WSP_block WSP_clearfix">
						<div class="WSP_float-left">
							<span class="WSP_text-bold">Donating</span>
						</div>
						<div class="WSP_float-right">
							<span class="WSP_text-grey WSP_inline-block WSP_right-arr js-donation-option"></span>
						</div>
					</div>
				</li>
				<li class="WSP_cursor-pointer js-feedback">
					<div class="WSP_block WSP_clearfix">
						<div class="WSP_float-left">
							<span class="WSP_text-bold">Feedback</span>
						</div>
					</div>
				</li>
			</ul>
			
			<ul class="WSP_list WSP_fixed-height upset">
				<li class="WSP_cursor-pointer js-logout-btn">
					<div class="WSP_block WSP_clearfix">
						${!this.guestMode ? `<span class="WSP_text-bold">Log Out</span> ` : ``}
                        ${this.guestMode ? `<span ><b>Start again</b> <span class="WSP_text-grey">(to select new charity)</span></span>` : ``}
					</div>
				</li>
			</ul>
			
			${this.guestMode ? `
            <ul class="WSP_list WSP_fixed-height upset">
                <li class="WSP_cursor-pointer js-login">
                    <div class="WSP_block ">
                        <div class="WSP_text-bold">Log In</div>
                    </div>
                </li>
            </ul>`:''}
			
			<div class="downset xlarge"></div>
			${!Config.default_search ? '<div class="downset xxlarge"></div>' : ''}
        </div>
        `
    }
}