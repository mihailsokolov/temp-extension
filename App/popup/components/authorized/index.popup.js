class IndexSplash extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        //If default organisation is selected redirect to entity selection
        if(this.rootScope.entity.id === Config.parent_organisation) {
            this.router.navigate('firstPage', {
                close:false
            });
            
            return
        }
        
        this.entity = this.rootScope.entity || {};
        
        this.guestMode = this.rootScope.settings.guestMode;
        
        this.elements();
        this.setEvents();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
  
        this.logo = new Logo(this.rootScope, {
            parent:this.find('.js-logo'), title:false, size:'large'
        });
        
        new ContributionTotal(this.rootScope, {
            parent: this.find('my-contribution')
        });
        
        new ContributionSwitcher(this.rootScope, {
            parent: this.find('contribution-switcher')
        });
    }
    
    setEvents() {
        this.$container.find('.js-open-settings').on('click', () => {
            this.destroy();
            this.router.navigate('settings');
        });
        
        this.find('.js-home-page').on('click', () => {
            this.entity.homepage ? window.open(this.entity.homepage) : '';
        })
    }
    
    domainPipe(domain) {
        if(!domain) {
            return '';
        }
        return domain.replace('http://', '').replace('https://', '').split('/')[0].replace('www.', '')
    }
    
    destroy() {
        this.$container.remove();
    }
    
    content() {
        return `
           <!-- Guest view -->
        <div class="WSP_bg-grey">
            <div class="WSP_text-right">
                <div class="WSP_fab WSP_fab_button WSP_cursor-pointer js-open-settings"
                        style="background: url('/assets/images/icons/menu.svg') no-repeat center #ffffff; top: 15px; right: 15px;"></div>
            </div>
        
            <div class="WSP_header">
             
                <div class="WSP_org-header inline-vertical-middle WSP_text-center bg-cover-center" 
                        style="${this.entity.background ? `background: url('${this.entity.background}') no-repeat center/contain; background-size: cover ` : ''}">
                    
                   
                    <div class="WSP_inline-block WSP_container-logo">
                        <div class="inline-vertical-middle">
                            <div class="js-logo"></div>
                        </div> 
                    </div>
                </div>
            </div>
            
            <ul class="WSP_list">
                <li class="js-home-page WSP_cursor-pointer">
                    <div class="WSP_block">
                        <div class="WSP_title"><b>${this.entity.title}</b></div>
                        <div class="WSP_text-xlsmall WSP_text-grey WSP_text-uppercase upset micro">
                            ${this.domainPipe(this.entity.homepage)}
                        </div>
                    </div>
                </li>
            </ul>
            
            <ul class="WSP_list  upset">
                <li>
                    <div class="WSP_block WSP_tiny WSP_clearfix">
                        <div class="WSP_float-left">
                            <div class="WSP_text-bold">Total raised</div>
                            <div class="WSP_text-grey WSP_text-xlsmall">By all supporters</div>
                        </div>
                        <div class="WSP_float-right WSP_text-bold upset tiny">${this.entity.symbol}${this.price(this.entity.raised)}</div>
                    </div>
                </li>
                
                <my-contribution></my-contribution>
                
            </ul>
            
            
            <contribution-switcher></contribution-switcher>
            
            <div class="offset large"></div>
            
        </div>
        
        `
    }
    
}