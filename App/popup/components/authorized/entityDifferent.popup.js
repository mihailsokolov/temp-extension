class EntityDifferentPopup extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
    }
    
    elements() {
        this.$container = $(this.content());
        
        //Yes
        this.yes = new SubmitBtn({
            parent: this.find('yes'),
            title:'Yes',
            callback: () => this.change(this.target)
        });
        //No
        this.no = new SubmitBtn({
            parent:this.find('no'),
            title:'No',
            grey: true,
            callback: () => this.change(this.current)
        });
        
        this.postOffice.get('getAllOrganisations').then(org => {
//            console.log(this.rootScope.ui)
            
            this.current = org.filter(o => o.id == this.rootScope.ui.settings.extension.entity)[0];
            this.target = org.filter(o => o.id == this.rootScope.ui.extension.entity.id)[0];
            
            if(!this.current || !this.target) {
                return;
            }
    
            this.find('target').text(this.target.title);
            this.find('current').text(this.current.title);
    
            this.reveal = new Reveal(this.rootScope, {
                content:this.$container,
                title: false
            });
        })
        
    }
    
    setEvents() {
        this.$container.on('submit', (e) => this.onSubmit(e));
    }
    
    change(target) {
        this.yes.state('busy');
        this.postOffice.post('updateEntity', target).then(() => {
            this.yes.state('success');
            
            setTimeout(() => {
                this.destroy();
            }, 1000)
        });
    }
    
    onSubmit(e) {
        e.preventDefault();
    
        this.postOffice.post('linkEmail', {
            email:this.$email.val()
        }).then(() => {
            this.error.destroy();
            this.rootScope.notifications.show('', `You were linked to “”.`)
        }, error => {
            this.submitBtn.state('error');
            this.error.show(error.email[0]);
            this.setInputError();
        })
    }
   
    destroy() {
        this.$container.remove();
        this.reveal.destroy();
    }
    
    
    content() {
        return `
        <div style="width: auto;">
        
            <div class="js-error-container" style="position: relative"></div>
        
            <div class="WSP_details-info WSP_text-left">
                
                <h2 class="WSP_text-header">You were previously linked to “<current></current>”.</h2>
            
                <h4 >
                     Do you want to update to “<target></target>”?
                </h4>
            </div>     
            
            <div class="WSP_details-input WSP_text-right js-submit-btn-container">
                <span style="padding-right: 1rem"><yes></yes></span>
                <no></no>
            </div>
            
        </div>
        
        `
    }
}