class DonatingPagePopup extends App {
	constructor(rootScope) {
		super(rootScope);
		
		this.show();
	}
    
    show() {
	    this.elements();
        this.setEvents();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
    
        /**
         * Current component elements
         */
        this.logo = new Logo(this.rootScope, {
            parent: this.$container.find('.js-logo')
        });
        
        this.list = new List(this.rootScope, {
            parent: this.$container.find('settings-list'),
            data: [
                {title: 'Ask everytime', id: 1},
                {title: 'Always', id: 2},
                {title: 'Never', id: 0}
            ].map(o => {
                o.active = o.id == this.rootScope.settings.donating_everywhere;
                return o
            })
        })
    }

	setEvents() {
	    this.$container.find('.js-back').on('click', () => {
	        this.destroy();
	        this.router.navigate('settings');
        });
	    
	    this.list.on('select', (e) => {
	        this.list.unCheckAll();
            e.button.state('busy');
            this.list.changeActive(e)
            
            if(!this.busy) {
                this.busy = true;
                this.postOffice.post('settingsDonatingOption', e.id).then(() => {
                    setTimeout(() => {
                        this.busy = false;
                        e.button.state('success')
                    }, 500);
                })
            }
        })
	}
    
    destroy() {
        this.$container.remove();
    }
    
    content() {
		return `
           <!-- Guest view -->
        <div class="WSP_bg-grey WSP_donating-settings-page">
           
			<div class="WSP_block">
				<div class="WSP_inline-block WSP_container-logo">
					<div class="js-logo"></div>
				</div>
				<div class="WSP_float-right">
				    <button class="WSP_button js-back">back</button>
                </div>
			</div>
			
			<div class="offset little WSP_text-uppercase WSP_text-xsmall" style="padding-left: 17px;  letter-spacing: 1px;">
			    DONATING
            </div>
			
			
			<ul class="WSP_list">
				<li class="WSP_cursor-pointer js-donate-regularity">
					<div class="WSP_block WSP_clearfix">
						<settings-list></settings-list>
					</div>
				</li>
			</ul>
			
			<div class="downset xxlarge"></div>
			<div class="downset xxlarge"></div>
        </div>
        `
	}
}