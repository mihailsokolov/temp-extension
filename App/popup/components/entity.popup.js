class EntityPopup extends App {
    constructor(rootScope) {
        super(rootScope);
        
        
        this.show();
    }
    
    on(event, callback) {
        if(event === 'success') {
            this.successCallback = callback;
        }
    }
    
    emit(event) {
        if(this.successCallback && event === 'success') {
            this.successCallback();
        }
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        //List
        this.list = new List(this.rootScope, {
            parent: this.find('.js-list-container'),
            search: true,
            title: ''
        });
        
        this.postOffice.get('organisations').then(org => {
            this.organisations = org;
            
            this.list.setOptions({
                data: this.organisations
            })
        });
        
        //logo
        this.logo = new Logo(this.rootScope, {
            size: 'medium',
            parent: this.find('.js-logo'),
            title: true,
            inline: true,
            useDefault: true
        });
    }
    
    setEvents() {
        this.find('.js-continue').on('click', () => this.submit(0));
        
        this.list.on('select', e => this.submit(e));
        
        this.find('.js-back').on('click', e => {
            this.destroy();
            this.router.navigate('firstPage')
        })
    }
    
    submit(e) {
        e.button.state('busy')
        this.postOffice.post('setEntity', e).then(() => {

            if(!this.rootScope.ui['user']) {
                this.router.navigate('authChoice')
            } else {

                this.router.navigate('index')
            }

            this.destroy();
        });
    }
    
    destroy() {
        this.$container.remove();
    }
    
    content() {
        return `
        <div class="WSP_text-left WSP_popup-entity"> 
        
            <div class="WSP_block">
				<div class="WSP_inline-block WSP_container-logo">
					<div class="js-logo"></div>
				</div>
				
				<div class="WSP_float-right">
				    <button class="WSP_button js-back">back</button>
                </div>
			</div>
	
        
            <!--LIST CONTAINER-->
            <div class="js-list-container"></div>
        </div>
        `
    }
}