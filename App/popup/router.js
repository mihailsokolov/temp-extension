class Router extends App {
    
    constructor(rootScope) {
        super(rootScope);
        
        this.current = null;
        this.reveal = null;
    }
    
    navigate(page) {
        switch (page) {
            case 'firstPage':
                return this.current = new FirstPagePopup(this.rootScope);
                break;
            case 'entity':
                return this.current = new EntityPopup(this.rootScope);
                break;
            case 'login':
                return this.current = new LogInPopup(this.rootScope);
                break;
            case 'authChoice':
                return this.current = new SplashAuthChoicePage(this.rootScope);
                break;
            case 'index':
                return this.current = new IndexSplash(this.rootScope);
                break;
            case 'settings':
                return this.current = new SettingsPopup(this.rootScope);
                break;
            case 'donating':
                return this.current = new DonatingPagePopup(this.rootScope);
                break;
            case 'entityDifferent':
                return this.current = new EntityDifferentPopup(this.rootScope);
                break;

                
            default:
                console.error('No such routes ' + page);
        }
    }
}