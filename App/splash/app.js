class App {
    constructor(rootScope = null) {
        if (rootScope) {
            this.rootScope = rootScope;
            this.postOffice = rootScope.postOffice;
        }
        
        this._events = [];

        /**
         * main container element
         * @type {*}
         */
        this.$$wrapper = $('.WSP_wrapper');
        this.$$appWrapper = $('.js-wrapper');

        this._subs = [];
        
        this.router = rootScope.router;
        
        this.entity = rootScope.entity;
    }
    
    
    setInTheMiddle() {
        this.$$wrapper.find('.js-main-container').addClass('inline-vertical-middle')
    }
    
    setContentUp() {
        this.$$wrapper.find('.js-main-container').removeClass('inline-vertical-middle')
    }
    
    find(el) {
        return this.$container.find(el);
    }
    
    _on(e, callback) {
        this._events.push({e: e, callback: callback});
    }
    
    _emit(e, data) {
        const event = this._events.find(_e => _e.e === e);
        event ? event.callback(data) : console.error('No such event' + e);
    }
}