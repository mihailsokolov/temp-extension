class HelpPageSplash extends App {
    constructor(rootScope) {
        super(rootScope);
        
        
        this.questionButton = this.$$wrapper.find('.js-main-right-top-question');
        this.closeButton = this.$$wrapper.find('.js-main-right-top-close-question');
    
        this.questionButton.on('click', () => {
            this.questionButton.hide();
            this.closeButton.show();
            this.show();
            
            this.router.current.destroy();
        });
        
        this.closeButton.on('click', () => this.destroy());
    }

    show() {
        this.elements();
        this.setEvents();

        this.$$appWrapper.append(this.$container);

        this.setContentUp();
    }

    elements() {
        this.$container = $(this.content());


        /**
         * Logo
         */
        this.logo = new Logo(this, {
            size: 'large',
            inline: false,
            titleAppend: 'Extension',
            parent: this.find('ui-logo')
        });

        /**
         * Help content
         */
        this.help = new UiHelpPage(this.rootScope, {
            parent: this.find('ui-help')
        });
        
    }

    setEvents() {
        //Close page
        this.find('.js-select-cancel').on('click', () => this.destroy())
    }

    destroy() {
        this.$container.remove();
        this.questionButton.show();
        this.closeButton.hide();
        this.rootScope.init();
    }

    content() {
        return `
            <div class="WSP_help-page-splash WSP_text-center upset large">
            
                <ui-logo></ui-logo>
            
                <ul class="WSP_list upset large">
                    <li>
                    
                        <div class="WSP_block">
                        
                            <div class="WSP_row WSP_clearfix">
                            
                                <div class="WSP_float-left WSP_title">
                                    About the extension
                                </div>
                                
                                <div class="WSP_float-right">
                                    <div class="WSP_button js-select-cancel">Cancel</div>
                                </div>
                                
                            </div>
                            
                            <div class="WSP_row upset small">
                                <hr>
                            </div>
                                                        
                        
                        
                            <div class="WSP_row WSP_clearfix">
                                <ui-help></ui-help>
                            </div>
                        
                        </div>
                    
                    
                    </li>
                </ul>
                
            </div>
        `
    }
}