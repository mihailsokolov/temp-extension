class SplashSettings extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.$$appWrapper.append(this.$container);
        
        this.setContentUp();
    }
    
    elements() {
        this.$container = $(this.content());
        
        //If default organisation is selected redirect to entity selection
        if(!this.rootScope.entity.selected && !this.codePicker) {
            this.codePicker = this.router.navigate('codePicker', {
                close:false
            });
        }
        
        
        /**
         * Logo
         */
        this.logo = new Logo(this, {
            size:'large', inline:false, titleAppend:'Extension', parent:this.find('ui-logo')
        });
        
        /**
         * DONATING
         */
        this.$donating = this.$container.find('.js-donating');
        this.donating = new SplashDonating(this.rootScope);
        this.donating.show(this.$donating);
        
        /**
         * Supporting option
         */
        this._subs.push(this.postOffice.stream('_ui').subscribe(ui => {
            
        }));
        
        if(Config.default_search) {
            new DefaultSearchOptions(this.rootScope, {
                parent:this.find('default-search-container')
            })
        }
        
        new SplashSupportingOptions(this.rootScope, {
            parent:this.find('supporting-option')
        });
    }
    
    setEvents() {
        this.find('.js-done').on('click', () => window.close())
    }
    
    destroy() {
        this.$container.remove();
        this._subs.forEach(sub => sub.unsubscribe());
    }
    
    content() {
        return `
            <div class="WSP_settings-wrapper upset large">
                <div class="WSP_text-center WSP_settings-container ">
                
                    <ui-logo></ui-logo>
            
                    <div class="upset large">
                       <default-search-container></default-search-container>
                    </div>
                    
                    <ul class="WSP_list small defaultSitesFullScreen">
                        <li>
                            <div class="WSP_block">
                                <div class="js-donating"></div>
                                
                                <hr class="WSP_row upset medium"/>
                                
                                <div class="upset medium">
                                    <supporting-option></supporting-option>
                                </div>
                            </div>
                        </li>
                    </ul>
            
                  
                </div>
                <div class="upset inline-vertical-middle" style="width: 190px">
                    <button class="WSP_button-main WSP_cont-as-guest js-done">
                        Done
                    </button>
                </div>
                
            </div>
        `
    }
}