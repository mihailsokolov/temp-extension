class DefaultSearchOptions extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            parent: null
        };
        
        this.options = Object.assign(this.options, options);
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.options.parent.append(this.$container)
    }
    
    elements() {
        this.$container = $(this.content());
    
        this.$moreOptions = this.$container.find('.js-more-options');
    
        this.list = new List(this.rootScope, {
            parent: this.$container.find('.js-domain-list'),
        });
    
        this.postOffice.get('getDomains').then(domains => {
            /**
             * Domains list
             */
            this._domains = this.domainMap(domains);
            this.list.setOptions({
                data: this._domains.filter(domain => domain.active)
            })
        });
        
    }
    
    setEvents() {
        this.list.on('select', e => {
            if(this.busy) {
                return;
            }
            this.busy = true;
            this.list.unCheckAll();
            e.button.state('busy');
        
            this.postOffice.post('defaultSearchEngine', e).then(() => {
                this.busy = false;
                e.button.state('success');
                this.list.changeActive(e)
            }, () => {
                e.button.state('error');
            })
        });
    
        this.$moreOptions.on('click', () => {
//            console.log("more clicked")
            this.list.setOptions({
                data: this._domains
            })
        })
    }
    
    domainMap(domains) {
        return domains.map(d => {
            d.domain.organisation.active = d.is_default;
            d.domain.organisation.domain_id = d.domain_id;
            return d.domain.organisation
        });
    }
    
    destroy() {
        this.$container.remove();
    }
    
    content() {
        return `
        <div class="WSP_settings-block">

            <div class="WSP_settings-row">
                <div class="WSP_settings-header-wrapper">
                    <h4 class="WSP_settings-header">Default search engine</h4>
                    <div class="WSP_settings-header-hint">Use the search engine of your choice and still raise money
                    </div>
                </div>
            </div>

            <div class="WSP_settings-row js-domain-list"></div>

            <div class="WSP_settings-row">
                <div class="WSP_button-holder">
                    <div class="WSP_button js-more-options">More Options</div>
                </div>
            </div>

        </div>
        `
    }
}