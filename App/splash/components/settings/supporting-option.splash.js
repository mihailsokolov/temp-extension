class SplashSupportingOptions extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.ui = this.rootScope.ui;
        
        this.supporting = this.rootScope.entity;
        
        this.options = {
            parent:null
        };
        
        this.options = Object.assign(this.options, options);
        
        this.show();
        
        this._subs.push(//UI stream
            this.postOffice.stream('_ui').subscribe(ui => {
                if(!ui.ver) {
                    return
                }

                this.ui = ui;

                this.$buttonsContainer.find('*').detach();

                if(this.ui['user']) {
                    this.showOpenButton = this.entity.id != Config.parent_organisation;
                    this.$buttonsContainer.append(this.showOpenButton ? this.$open : this.$browseAll);
                    this.getSupporterDomain();
                } else {
                    //this.$buttonsContainer.append(this.$getStarted);
                }

            })
        )
        
        
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.options.parent.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        this.$buttonsContainer = this.find('buttons-container');
        
        this.$open = $(this.openBtn());
        this.$browseAll = $(this.browseAll());
        this.$getStarted = $(this.getStarted());
        
        this.getSupporterDomain();
        
        //Logo
        new Logo(this.rootScope, {
            size:'fit',
            parent:this.find('ui-logo'),
            useSupporting:true,
            title:true,
            inline:true
        });
    }
    
    setEvents() {
        this.$browseAll.on('click', () => {
            this.router.current.destroy();
            this.router.navigate('selectSupporting');
        });
        
        this.$open.on('click', () => window.open(this.supporterDomain, '_blank'));
        
        this.$getStarted.on('click', () => this.router.navigate('signUp'))
    }
    
    getSupporterDomain() {
        const map = (s) => s.indexOf('http://') === -1 && s.indexOf('https://') === -1 ? 'http://' + s : s;
        
        if(this.supporting.domains && this.supporting.domains.length) {
            this.supporterDomain = map(this.supporting.domains[0]);
        } else if(this.supporting['homepage']) {
            this.supporterDomain = map(this.supporting['homepage']);
        } else {
            this.$open.detach();
        }
    }
    
    destroy() {
        this.$container.remove();
        this._subs.forEach(s => s.unsubscribe());
    }
    
    openBtn() {
        return `<button class="WSP_button">Open</button>`
    }
    
    browseAll() {
        return `<button class="WSP_button">Browse All</button>`
    }
    
    getStarted() {
        return `<button class="WSP_button">GET STARTED</button>`
    }
    
    content() {
        return `
            <div >
                <div class="WSP_text-left">
                    <h4 class="WSP_title">Supporting</h4>
                    <div class="upset micro">Select browse all to pick the charity you’d like to support</div>
                </div>
                <div class="WSP_clearfix upset">
                    <div class="WSP_float-left">
                        <ui-logo></ui-logo>
                    </div>
                    
                    
                    <div class="WSP_float-right upset small">
                        <buttons-container></buttons-container>
                       
                    </div>
                </div>
            </div>
        `
    }
}