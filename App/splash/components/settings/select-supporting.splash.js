class SelectSupportingPageSplash extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.setContentUp();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        new Logo(this, {
            size: 'large',
            parent: this.find('ui-logo'),
            inline: false
        });
    
        this.list = new List(this.rootScope, {
            parent: this.find('.js-list-container'),
            search: true,
        });
    
        this.postOffice.get('organisations').then(org => {
            this.organisations = org;
            
            if(this.rootScope.ui.extension.supporting) {
                let org = this.organisations.find(o => o.id === this.rootScope.ui.extension.supporting.id);
                org ? org.active = true : '';
            }
    
            
            //List
            this.list.setOptions({
                data: this.organisations
            })
        });
        
    }
    
    setEvents() {
        this.$container.find('.js-cancel').on('click', () => {
            this.destroy();
            this.rootScope.init();
        });
        
        this.list.on('select', e => this.submit(e))
    }
    
    submit(e) {
        this.postOffice.post('setSupporting', e);
        this.destroy();
        this.rootScope.init();
    }
    
    
    destroy() {
        this.$container.remove();
    }
    
    content() {
        return `
            <div class="WSP_settings-wrapper  upset large">
                <div class="WSP_text-center WSP_settings-container ">
                    <ui-logo></ui-logo>
            
                    <div class="upset large"></div>
                  
                    <ul class="WSP_list small defaultSitesFullScreen">
                    
                        <li>
                            <div class="WSP_block defaultSitesFullScreen-org-list">
                                <div class="WSP_clearfix">
                                    <h4 class="WSP_title upset micro WSP_float-left">
                                       Browse all charities
                                    </h4>
                                    <div class="WSP_float-right" style="margin-right: -10px">
                                        <div class="WSP_button js-cancel">
                                            Cancel
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="WSP_row js-list-container upset small"></div>
                            </div>
                           
                        </li>
                    </ul>
                </div>
            </div>
        `
    }
}