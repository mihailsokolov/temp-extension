class SplashDonating extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            parent: null
        };
        
        this.options = Object.assign(this.options, options);
    }
    
    show(parent) {
        this.elements();
        this.setEvents();
        
        parent.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        this.$btns = this.$container.find('.js-donating-btn');
        
        this.postOffice.stream('_settings').subscribe(settings => {
            this.donating = settings.donating_everywhere;
            this.$btns.removeClass('WSP_button-active');
            this.$btns.each((key, btn) => {
                btn = $(btn);
                if(btn.attr('data-id') == this.donating) {
                    btn.addClass('WSP_button-active');
                }
            })
        })
        
    }
    
    setEvents() {
        this.$btns.on('click', (e) => {
            this.$btns.removeClass('WSP_button-active');
            const target = $(e.currentTarget);
            target.addClass('WSP_button-active');
            this.postOffice.post('settingsDonatingOption', target.attr('data-id'))
        })
    }
    
    destroy() {
        this.$container.remove();
        //this._subs.forEach(sub => sub.unsubscribe());
    }
    
    content() {
        return `
            <div class="WSP_text-left">
                <h4 class="WSP_title">
                    Donating via extension
                </h4>
                <div class="upset micro">
                    When visiting sites or using other search engines we need your
                    permission to donate
                </div>
            </div>  
            <div class="WSP_row upset WSP_button-holder-justified">
                <div class="WSP_button js-donating-btn" data-id="2">Always</div>
                <div class="WSP_button js-donating-btn" data-id="1">Ask everytime</div>
                <div class="WSP_button js-donating-btn" data-id="0">Never</div>
            </div>
        `
    }
}