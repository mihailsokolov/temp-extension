class GlobalEvents extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.$logout = this.$$wrapper.find('.js-main-page-logout');
        
        this.logOutButton();
        this.setEvents();
        
        this.user = null;
    }
    
    setEvents() {
        this.postOffice.stream('_ui').subscribe(ui => this.renderButton('ui'));
        
        this.postOffice.stream('_settings').subscribe((s) => this.renderButton('settings'));
        
        this.postOffice.setEvent('authorized', (a) => {
            
//            if(this.ui.user || this.settings.guestMode) {
//                if(this.router.current.constructor.name === 'SplashSettings') {
//                    return;
//                }
//
//                this.router.current.destroy();
//
//                this.router.navigate('settings');
//            } else {
//                this.router.current.destroy();
//                this.router.navigate('firstPage');
//            }
        })
    }
    
    renderButton(t) {
        this.settings = this.rootScope.settings || {};
        this.ui = this.rootScope.ui;
        
        this.ui['user'] || this.settings.guestMode ? this.$logout.show() : this.$logout.hide();
        
        this.$logout.text(this.settings.guestMode ? 'Start again (to select new charity)' : 'Logout')
    }
    
    logOutButton() {
        this.$logout.on('click', () => {
            
            this.postOffice.post('logout', {}).then(() => {
                this.renderButton();
                this.router.current.destroy();
                this.rootScope.init();
            })
        });
    }
}