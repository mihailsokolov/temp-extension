class SplashCodePicker extends App {
    constructor(rootScope, options) {
        super(rootScope);
        
        this.options = {
            close: true
        };
        
        this.options = Object.assign(this.options, options);

        
        this.show();
    }

    show() {
        this.elements();
        this.setEvents();
    
        this.reveal = new Reveal(this.rootScope, {
            content: this.$container,
            header: false,
            width: '580px',
            close: this.options.close
        });
    }
    
    on(event, callback) {
        if(event === 'success') {
            this.successCallback = callback;
        }
    }
    
    emit(event) {
        if(this.successCallback && event === 'success') {
            this.successCallback();
        }
    }

    elements() {
        this.$container = $(this.content());
    
        this.logo = new Logo(this.rootScope, {
            size: 'large',
            parent: this.$container.find('ui-logo'),
            inline: false,
            titleAppend: 'Extension'
        });
        
        this.codePicker = new UiCodePicker(this.rootScope, {
            parent: this.find('ui-code-picker')
        });
    }

    setEvents() {
        this.codePicker.on('success', () => {
            this._emit('success');
            
            this.destroy();
        });
        
        this.codePicker.on('error', (error) => this.rootScope.errors.show(error.message || error));
        this.codePicker.on('changed', () => this.rootScope.errors.destroy());
        
        //If user select search manual
        this.$container.find('.js-manual-search').on('click', () => {
            this.destroy();
            
            this.defaultSites = this.router.navigate('entityPopup', this.options);
        })
    }

    destroy() {
        this.$container.remove();
        this.rootScope.errors.destroy();
    
        this.reveal.destroy();
    }

    content() {
        return `
            <div class="offset xlarge">
                <ui-logo></ui-logo>
                <div class="WSP_text-center WSP_text-grey downset large">
                    Create an account
                </div>
                
                <ui-code-picker></ui-code-picker>
                
                <div class="upset">
                    <a class="WSP_text-grey WSP_text-small js-manual-search">SEARCH MANUALLY</a>
                </div>
            </div>
        `
    }
}



