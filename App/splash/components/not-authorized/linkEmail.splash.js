class LinkEmailSplash extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
    }
    
    get() {
        this.elements();
        this.setEvents();
        return this.$container;
    }
    
    elements() {
        this.$container = $(this.content());
        
        //Inputs
        this.$email = this.$container.find('.js-email-input');
        
        //Submit button
        this.submitBtn = new SubmitBtn({
            parent:this.find('.js-submit-btn-container'), title:'Finish'
        });
        
        //Errors
        this.error = new Errors(this.rootScope, {
            parent:this.find('.js-error-container'), position:'top'
        });
        
        this.reveal = new Reveal(this.rootScope, {
            content:this.$container, title:'Create an account'
        });
    }
    
    setEvents() {
        this.$container.on('submit', (e) => this.onSubmit(e));
        
        this.$email.on('keydown', () => {
            if(this.$email.hasClass('WSP_error-input')) {
                this.submitBtn.state('default');
                this.removeInputError();
                this.error.destroy();
            }
        });
    }
    
    onSubmit(e) {
        e.preventDefault();
        
        if(this.$email.val().length > 3) {
            
            this.submitBtn.state('busy');
            
            
            this.postOffice.post('linkEmail', {
                email:this.$email.val()
            }).then(() => {
                this.error.destroy();
                this.submitBtn.state('success');
                this.removeInputError();
                
                setTimeout(() => this.destroy(), 2000);
                
                this.rootScope.notifications.show('Email', 'Your email was added. Check your email')
            }, error => {
                this.submitBtn.state('error');
                this.error.show(error.email[0]);
                this.setInputError();
            })
        } else {
            this.destroy()
        }
    }
    
    setInputError() {
        this.$email.addClass('WSP_error-input')
    }
    
    removeInputError() {
        this.$email.removeClass('WSP_error-input')
    }
    
    destroy() {
        this.$container.remove();
        this.rootScope.errors.destroy();
        this.reveal.destroy();
    }
    
    
    content() {
        return `
        <form style="width: 580px; overflow: hidden" class="js-recovery-form">
        
            <div class="js-error-container" style="position: relative"></div>
        
            <div class="WSP_details-info WSP_text-left">
            
                <h4 class="WSP_text-header">
                    Link your email <span class="WSP_text-header-hint">(optional)</span>
                </h4>
                
                <p class="WSP_details-info-small">
                    You can only reset your password if an email is linked to your account.
                </p>
            </div>     
            
            <div class="WSP_details-input">
                <input type="text" class="WSP_main-input js-email-input" placeholder="E-MAIL">
                <span class="val-icon"></span>
            </div>
            
            
            <div class="WSP_details-input WSP_text-right js-submit-btn-container"></div>
            
        </form>
        
        `
    }
}