class SplashFirstPage extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.setInTheMiddle();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        this.logo = new Logo(this.rootScope, {
            size: 'large',
            parent: this.$container.find('ui-logo'),
            inline: false,
            titleAppend: 'Extension'
        });
        
        this.codePicker = new UiCodePicker(this.rootScope, {
            parent: this.find('ui-code-picker')
        })
        
        
    }
    
    setEvents() {
        this.codePicker.on('error', error => this.rootScope.errors.show(error.message || error));
        
        this.codePicker.on('changed', () => this.rootScope.errors.destroy());
        
        this.codePicker.on('success', () => {
            this.rootScope.errors.destroy();
            this.router.navigate('authChoice');
            this.destroy();
        });
        
        this.find('.js-login').on('click', () => {
            this.destroy();
            this.router.navigate('login')
        });
        
        this.find('.js-manual').on('click', () => {
            this.entity = this.router.navigate('entity');
            this.entity._on('success', () => {
                this.destroy();
                this.router.navigate('authChoice')
            })
        })
    }
    
    destroy() {
        this.$container.remove();
        this._subs.forEach(sub => sub.unsubscribe());
    }
    
    content() {
        return `
        <!--Login wrapper-->
        <div class="WSP_login-page js-login-page WSP_text-center">
            <ui-logo></ui-logo>
                
            <hr class="upset small">
            
            <div class="WSP_text-small" style="line-height: 2; margin: 0 -1.9rem">
                Thanks for downloading the extension, first we just need to know <br> which charity you’re supporting. 
                If you know your code please<br> enter it below or search manually.
            </div>
         
            <div class="upset">
                <ui-code-picker></ui-code-picker>
            </div>
            
            <div class="WSP_text-xsmall upset small" style="letter-spacing: 1px;">
                DON’T KNOW YOUR CODE? <a class="WSP_cursor-pointer WSP_text-bold js-manual">SEARCH MANUALLY</a>
            </div>
            
            <hr>
            
            <div>
                <button class="WSP_button-main WSP_grey js-login" style="background-color: #f1f2f4;">
                    Login
                </button>
            </div>
        </div>
        `
    }
}