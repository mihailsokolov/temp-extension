class SplashAuthChoicePage extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.setInTheMiddle();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        this.logo = new Logo(this.rootScope, {
            size: 'large',
            parent: this.$container.find('ui-logo'),
            inline: false,
            titleAppend: 'Extension'
        });
        
    }
    
    setEvents() {
        
        this.find('.js-login').on('click', () => {
            this.destroy();
            this.router.navigate('login');
        });
        
        this.find('.js-signup').on('click', () => {
            this.router.navigate('signUp')
        });
        
        this.find('.js-guest').on('click', () => {
            this.postOffice.post('guestMode', true).then(() => this.goNext());
        });
        
        this.find('.js-back').on('click', () => {
            this.postOffice.post('destroyEntity', {}).then(() => {
                this.destroy();
                this.router.navigate('firstPage')
            });
        })
    }
    
    goNext() {
        this.destroy();
        if(this.rootScope.entity.selected) {
            this.router.navigate('settings');
        } else {
            this.router.navigate('settings');
            this.router.navigate('codePicker');
        }
    }
    
    destroy() {
        this.$container.remove();
        this._subs.forEach(sub => sub.unsubscribe());
    }
    
    content() {
        return `
        <!--Login wrapper-->
        <div class="WSP_login-page js-login-page WSP_text-center">
        
            <div class="WSP_button WSP_grey WSP_text-uppercase js-back"
             style="position: absolute; left: 15px; top: 15px;">back</div>
        
            <ui-logo></ui-logo>
                
            <hr class="upset small">
            
            <div class="WSP_text-small" style="line-height: 2; margin: 0 -1.9rem">
                Create an account to keep track of how much you’ve raised when<br> shopping online. 
                Guest accounts still generate donations but your<br> personal contribution total will not be available
            </div>
         
            <div class="upset large">
                <button class="WSP_button-main js-signup">
                    CREATE ACCOUNT
                </button>
            </div>
            
            <div class="WSP_hr-or offset medium">OR</div>
            
            <div>
                <button class="WSP_button-main WSP_grey js-guest" style="background-color: #f1f2f4;">
                    CONTINUE AS GUEST
                </button>
            </div>
            
            <div class="WSP_text-xsmall upset" style="letter-spacing: 1px;">
                ALREADY HAVE AN ACCOUNT? <a class="WSP_cursor-pointer WSP_text-bold js-login">LOG IN</a>
            </div>
            
        </div>
        `
    }
}