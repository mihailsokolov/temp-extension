class SplashRecovery extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }

    show() {
        this.elements();
        this.setEvents();
    }
    
    elements() {
        this.$container = $(this.content());
        this.$form = this.$container.find('.js-recovery-form');
        //Cancel button
        this.$email = this.find('.js-email-input');
        
        //Submit button
        this.submitBtn = new SubmitBtn({
            parent: this.find('.js-submit-btn-container')
        });
        
        //Error
        this.error = new Errors(this.rootScope, {
            parent: this.find('.js-error-container'),
            position: 'top'
        });
    
        this.reveal = new Reveal(this.rootScope, {
            content: this.$container,
            title:'Forgotten details'
        });
        
    }
    
    setEvents() {
        this.$container.on('submit', (e) => this.onSubmit(e));
        
        this.$email.on('keydown', () => {
            if(this.$email.hasClass('WSP_error-input')) {
                this.submitBtn.state('default');
                this.removeInputError();
                this.error.destroy();
            }
        });
    }
    
    onSubmit(e) {
        e.preventDefault();
        
        if(this.$email.val().length > 3) {
            
            this.submitBtn.state('busy');
            
            this.postOffice.post('doRecovery', {
                email:this.$email.val()
            }).then(() => {
                this.error.destroy();
                this.submitBtn.state('success');
                this.removeInputError();
            }, error => {
                
                this.submitBtn.state('error');
                this.error.show(error.email[0]);
                this.setInputError();
            })
        } else {
            this.setInputError();
        }
    }
    
    setInputError() {
        this.$email.addClass('WSP_error-input')
    }
    
    removeInputError() {
        this.$email.removeClass('WSP_error-input')
    }
    
    destroy() {
        this.$container.remove();
        this._subs.forEach(sub => sub.unsub());
        this.rootScope.errors.destroy();
        this.loginForm.destroy();
    }
    
    
    content() {
        return `
        <form style="width: 580px; overflow: hidden" class="js-recovery-form">
        
            <div class="js-error-container" style="position: relative"></div>
        
            <div class="WSP_details-info WSP_text-left">
                <p>
                    Enter the email that is linked to your account to receive an email 
                    with a link to reset your password. If you don’t have an email linked 
                    to your account your password cannot be reset.
                </p>
            </div>     
            
            <div class="WSP_details-input">
                <input type="text" class="WSP_main-input js-email-input" placeholder="E-MAIL">
                <span class="val-icon"></span>
            </div>
            
            <div class="WSP_details-input WSP_text-right js-submit-btn-container"></div>
            
        </form>
        
        `
    }
}