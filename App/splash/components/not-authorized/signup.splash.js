class SplashSignUp extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }

    show() {
        this.elements();
        this.setEvents();
    }

    elements() {
        this.$container = $(this.content());
        this.$form = this.$container.find('.js-form-container');
        
        this.$back = this.$container.find('.js-back');
        
        //Sign up form
        this.uiSignUp = new UiSignUp(this.rootScope);
        this.$form.append(this.uiSignUp.get());
    
        //Error
        this.error = new Errors(this.rootScope, {
            parent: this.find('.js-error-container'),
            position: 'top'
        });
    
        this.reveal = new Reveal(this.rootScope, {
            content: this.$container,
            title:'Create an account'
        });
    }

    setEvents() {
        this._subs = [];
    
        this.uiSignUp.on('error', error => this.error.show(error));
        this.uiSignUp.on('success', () => {
            this.destroy();
    
            this.rootScope.router.current.destroy();
    
            this.rootScope.router.navigate('settings');
    
            this.rootScope.router.navigate('linkEmail');
            
            this.rootScope.notifications.show('Welcome to ' + Config.name, 'Your account was created successfully')
        });
    
        this.$back.on('click', e => {
            this.destroy();
        })
    }

    destroy() {
        this.$container.remove();
        this.rootScope.errors.destroy();
        this.reveal.destroy();
    }

    content() {
        return `
        <div style="position: relative; overflow: hidden;">
            <div class="WSP_ext-create-acc js-error-container"></div>
            <div class="WSP_ext-create-acc js-form-container"></div>
            
            <button class="WSP_button-main WSP_smaller WSP_grey  WSP_float-left js-back" 
                    style="position: absolute; left: 20px; bottom: 20px">Back</button>
        </div>
        
        `
    }
}



