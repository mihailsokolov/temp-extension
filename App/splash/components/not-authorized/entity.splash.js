class SplashEntitySites extends App {
    constructor(rootScope) {
        super(rootScope);
        
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
    }
    
    elements() {
        this.$container = $(this.content());
        
        //List
        this.list = new List(this.rootScope, {
            parent: this.$container.find('.js-list-container'),
            search: true,
            title: 'Search for the organisation you want to support',
            searchPlaceholder: 'Search'
        });
        
        this.postOffice.get('organisations').then(org => {
            this.organisations = org;
            
            this.list.setOptions({
                data: this.organisations
            })
        });
        
        //logo
        this.logo = new Logo(this.rootScope, {
            size: 'medium',
            parent: this.$container.find('.js-logo'),
            title: true,
            inline: true,
            titlePrepend: 'Continue as',
            useDefault: true
        });
        
        this.reveal = new Reveal(this.rootScope, {
            content: this.$container,
            title:'Create an account',
            width:'580px'
        });
    }
    
    setEvents() {
        this.find('.js-continue').on('click', () => this.submit(0));
        
        this.list.on('select', e => this.submit(e));
        
        this.find('.js-back').on('click', () => this.destroy());
    }
    
    submit(e) {
        this.postOffice.post('setEntity', e);
        
        this._emit('success');
        
        this.destroy();
    }
    
    destroy() {
        this.$container.remove();
        this.reveal.destroy();
    }
    
    content() {
        return `
        <!--Login wrapper-->
        <div class="WSP_text-center"> 
            <!--LIST CONTAINER-->
            <div class="js-list-container"></div>
            
            <div class="WSP_header-small WSP_text-left" 
                style="
                        border-top: solid 1px #efefef; 
                        height: ${Config.defaultSites_continue_with_default ? '96' : '74'}px; 
                        position: relative;
                ">
                
                <!--CONTINUE AS default-->
                ${Config.defaultSites_continue_with_default ? `

                    <div class="js-logo WSP_float-left" style="line-height: 0; margin-top: 3px;"></div>
                                        
                    <button class="WSP_button-main WSP_top-right-button WSP_smaller WSP_grey js-continue" 
                            style="right: 20px;top: 25px;">
                        CONTINUE
                    </button>
                    
                `: `

                    <button class="WSP_button-main WSP_smaller WSP_grey  WSP_float-left js-back" 
                            style="position: absolute; left: 20px; bottom: 20px">Back</button>

                `}
               
            </div>

            
            
            
        </div>
        `
    }
}