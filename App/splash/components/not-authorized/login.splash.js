class SplashLogInPage extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.setInTheMiddle();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        
        this.logo = new Logo(this.rootScope, {
            size:'large', parent:this.find('.js-logo'), inline:false, titleAppend:'Extension'
        });
        
        this.loginForm = new LoginForm(this.rootScope, {
            parent: this.find('.js-login-submit-form')
        });
    }
    
    setEvents() {
        //Cancel
        this.find('.js-login-cancel').on('click', () => {
            this.rootScope.notAuthorized.show();
            this.destroy();
        });
        
        this.find('.js-recovery').on('click', () => {
            this.router.navigate('recovery')
        });
        
        this.find('.js-create').on('click', () => {
            this.signup = this.router.navigate('signUp')
        });
        
        this.loginForm.on('error', (message) => {
            this.rootScope.errors.show(message)
        });
        
        this.loginForm.on('success', () => this.goNext());
        
        this.find('.js-continue-guest').on('click', () => {
            this.postOffice.post('guestMode', true).then(() => this.goNext());
        });
        
        this.find('.js-back').on('click', () => {
            this.destroy();
            this.rootScope.init();
        })
    }
    
    goNext() {
        
        this.destroy();
        this.router.navigate('settings');
    }
    
    destroy() {
        this.$container.remove();
        this._subs.forEach(sub => sub.unsubscribe());
        this.rootScope.errors.destroy();
        this.loginForm.destroy();
    }
    
    content() {
        return `
        <!--Login wrapper-->
        <div class="WSP_login-page js-login-page WSP_text-center">
        
            <button class="WSP_button-main WSP_smaller js-back" 
                    style="position: absolute; left: 20px; top: 20px">Back</button>
        
            <div class="WSP_login-content">
                <div class="js-logo"></div>
             
                <div class="js-login-submit-form"></div>
                
                <div>
                    <button class="WSP_button-main WSP_grey WSP_cont-as-guest js-continue-guest" style="background-color: #f1f2f4;">
                        continue as guest
                    </button>
                </div>
                
                <div class="WSP_login-navigation">
                    <a class="js-create">CREATE AN ACCOUNT</a>
                    <a class="js-recovery">FORGOTTEN DETAILS</a>
                </div>
               
            </div>
        </div>
        `
    }
}