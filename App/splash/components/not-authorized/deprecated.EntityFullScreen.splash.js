class DefaultSitesFullScreen extends App {
    constructor(rootScope) {
        super(rootScope);
    }
    
    show() {
        this.elements();
        this.setEvents();
        
        this.setContentUp();
        
        this.$$appWrapper.append(this.$container);
    }
    
    elements() {
        this.$container = $(this.content());
        this.$logo = this.$container.find('.js-logo');
        this.$logoSmall = this.$container.find('.js-logo-small');
        this.$cancel = this.$container.find('.js-cancel');
        this.$selectDefault = this.$container.find('.js-select-default');
        
        new Logo(this, {
            size: 'large',
            parent: this.$logo,
            inline: false
        });
        
        new Logo(this, {
            size: 'fit',
            useDefault: true,
            titleAppend: 'Extension',
            parent:  this.$logoSmall
        });
    
        this.list = new List(this.rootScope, {
            parent: this.$container.find('.js-list-container'),
            search: true,
        });
    
        this.postOffice.get('organisations').then(org => {
            this.organisations = org;
    
            //List
            this.list.setOptions({
                data: this.organisations
            })
        });
        
    }
    
    setEvents() {
        this.$cancel.on('click', () => {
            this.destroy();
            this.postOffice.post('guestMode', false)
            this.rootScope.showLogin();
        });
    
        this.$selectDefault.on('click', () => this.submit(0));
        
        this.list.on('select', e => this.submit(e))
    }
    
    submit(e) {
        this.postOffice.post('defaultSites', e);
        this.destroy();
        this.rootScope.showSettings();
    }
    
    
    destroy() {
        this.$container.remove();
    }
    
    content() {
        return `
            <div class="WSP_settings-wrapper  upset large">
                <div class="WSP_text-center WSP_settings-container ">
                    <div class="js-logo"></div>
            
                    <div class="upset large"></div>
                  
                    <ul class="WSP_list small defaultSitesFullScreen">
                    
                        <div class="WSP_button WSP_cancel-button js-cancel">
                            Cancel
                        </div>
                    
                        <li>
                            <div class="WSP_block WSP_text-left">
                                <h4 class="WSP_title">
                                    Which extension are you using?
                                </h4>
                                <div class="upset tiny">
                                    WhaleSlide users should select WhaleSlide before choosing the charity they’d like to support. 
                                    If you’re using one of our WhiteLabel search engines select them from the list below. 
                                </div>
                            </div>
                        </li>
                        
                        
                        <li>
                        
                            <div class="WSP_block">
                                
                                <div class="WSP_row WSP_clearfix">
                                
                                    <div class="WSP_float-left js-logo-small"></div>
                                    
                                    <div class="WSP_float-right upset small">
                                        <div class="WSP_button js-select-default">
                                            Select
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </li>
                        
                        
                        <li>
                            <div class="WSP_block defaultSitesFullScreen-org-list">
                                <div class="WSP_clearfix">
                                    <h4 class="WSP_title upset micro WSP_float-left">
                                       Browse all charities
                                    </h4>
                                    <div class="WSP_float-right" style="margin-right: -10px">
                                        <div class="WSP_button js-cancel">
                                            Cancel
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="WSP_row js-list-container upset small"></div>
                            </div>
                           
                        </li>
                    </ul>
                </div>
            </div>
        `
    }
}