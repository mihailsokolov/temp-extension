class Router extends App {
    
    constructor(rootScope) {
        super(rootScope);
        
        this.current = null;
        this.reveal = null;
    
        this.hash = window.location.hash.replace("#", '');
    
        if(this.hash && this.hash.length) {
            this.navigate(this.hash);
        }
        
    }
    
    navigate(page, options = null) {
        switch (page) {
            case 'firstPage':
                return this.current = new SplashFirstPage(this.rootScope);
                break;
            case 'authChoice':
                return this.current = new SplashAuthChoicePage(this.rootScope);
                break;
            case 'settings':
                return this.current = new SplashSettings(this.rootScope);
                break;
            case 'login':
                return this.current = new SplashLogInPage(this.rootScope);
                break;
            case 'selectSupporting':
                return this.current = new SelectSupportingPageSplash(this.rootScope);
                break;
    
    
            case 'codePicker':
                return this.reveal = new SplashCodePicker(this.rootScope, options);
                break;
            case 'linkEmail':
                return this.reveal = new LinkEmailSplash(this.rootScope);
                break;
            case 'entityPopup':
                return this.reveal = new EntityPopupSplash(this.rootScope, options);
                break;
            case 'recovery':
                return this.reveal = new SplashRecovery(this.rootScope);
                break;
            case 'signUp':
                return this.reveal = new SplashSignUp(this.rootScope);
                break;
            case 'entity':
                return this.reveal = new SplashEntitySites(this.rootScope);
                break;
            case 'entityDifferent':
                return this.reveal = new EntityDifferentSplash(this.rootScope, options);
                break;
            default:
                console.error('No such routes');
        }
    }
}