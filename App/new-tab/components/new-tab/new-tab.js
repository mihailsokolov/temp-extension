const LEFT_CODE = 37;
const UP_CODE = 38;
const RIGHT_CODE = 39;
const DOWN_CODE = 40;
const ENTER_CODE = 13;

class NewTab {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.extractDomain = this.rootScope.extractDomain;
        
        this.$sitesWrapper = $('.WSP_pinned-sites-wrapper');
        
        this.$searchBar = $('.WSP_searchline');
        this.$searchInput = this.$searchBar.find('input');
        this.$searchButton = this.$searchBar.find('button');
        
        this.$suggestionsContainer = $('.WSP_search-suggestions');
        
        this.suggesting = false;
        this.currentSuggestion = -1;
        this.suggestionItem = 'WSP_search-suggestion';
        
        this.$searchInput.on('click', (e) => {
            let input = this.$searchInput.val().trim();
            
            if(input) {
                this.suggestions(e);
            }
        });
        
        this.$searchButton.on('click', () => this.search());
        this.$searchInput.on('keyup', (e) => {
            if(e.keyCode === UP_CODE) {
                this.prevSuggestion();
            }
            
            if(e.keyCode === DOWN_CODE) {
                this.nextSuggestion();
            }
            
            if(e.keyCode === ENTER_CODE) {
                this._setSuggestionValue();
                
                this.search();
            }
            
            this.suggestions(e);
        });
        
        this.history();
        
        this._registerEvents();
    }
    
    history() {
		let resolver = new Promise((resolve) => chrome.history.search({
			'text': '',
			'maxResults': 100
		}, (historyItems) => {
			resolve(historyItems);
		}));

        resolver.then((data) => {
			let history = Object.values(data).sort(function(a,b){
			    return b.visitCount - a.visitCount;
			});

			history = history.slice(0, 8);

			history.map((historyItem) => {
                const content = this._render(historyItem);

				this.$sitesWrapper.append(content);

				this._getThumbnail(historyItem.id);
            });
        });
    }
    
    suggestions(e) {
        const input = $(e.target).val().trim();
        
        if(!input || this.suggesting) return;
        if([UP_CODE, DOWN_CODE, LEFT_CODE, RIGHT_CODE].indexOf(e.keyCode) > -1) return;
        
        this.suggesting = true;
        
        this.$suggestionsContainer.empty();
        this._suggestionsShow();
        
        $.get(`https://search.whaleslide.com/api/v1/suggestions/get?q=${input}&num=5`, (data) => {
            data.map((suggestion) => {
                this.$suggestionsContainer.append(this._suggestion(suggestion));
            });
            
            this.suggesting = false;
        });
    }
    
    search() {
        this._suggestionsHide();
        
        let query = this.$searchInput.val();
        
        window.location.href = `https://whaleslide.com/search/web/${query}`;
    }
    
    nextSuggestion() {
        this.currentSuggestion++;
        
        if(this.currentSuggestion > 4) this.currentSuggestion = 4;
        
        this._suggestionStep();
    }
    
    prevSuggestion() {
        this.currentSuggestion--;
        
        if(this.currentSuggestion < 0) this.currentSuggestion = 0;
        
        this._suggestionStep();
    }
    
    _setSuggestionValue() {
        let suggestion = this._getSuggestion();
        
        if(suggestion) {
            this.$searchInput.val(suggestion.text());
        }
    }
    
    _getSuggestion() {
        if(this.currentSuggestion === -1) return null;
        
        return this.$suggestionsContainer
        .find('.' + this.suggestionItem)
        .eq(this.currentSuggestion);
    }
    
    _suggestionStep() {
        this._getSuggestion()
        .addClass(this.suggestionItem + '--active')
        .siblings()
        .removeClass(this.suggestionItem + '--active');
    }
    
    _suggestionsShow() {
        this.$suggestionsContainer.addClass('WSP_search-suggestions--active');
    }
    
    _suggestionsHide() {
        this.currentSuggestion = -1;
        this.suggesting = false;
        
        this.$suggestionsContainer
        .removeClass('WSP_search-suggestions--active')
        .empty();
    }
    
    _suggestion(suggestion) {
        return `<div class="WSP_search-suggestion">${suggestion}</div>`;
    }
    
    _render(site) {
        site = this._prepare(site);
        
        return `<div class="WSP_pinned-site-container" 
				data-id="${site.id}" 
				data-domain="${site.domain}"
				data-link="${site.url}">
            <div class="WSP_remove"></div>
            <a href="${site.url}" title="${site.title}">
				<div class="WSP_pinned-pic"></div>
				<div class="WSP_pinned-url-container">
					<img src="${site.favicon}" alt="${site.fullTitle}" class="WSP_domain-favicon">
					<span class="WSP_text-bold">${site.title}</span>
				</div>
            </a>
        </div>`;
    }
    
    _prepare(site) {
        let id = site.id;
        let domain = this.extractDomain.extractRootDomain(site.url);
        let title = site.title.length > 0 ? site.title : domain;
        let fullTitle = site.title;
        let favicon = `https://icnt.ch/icon?domain=${domain}`;
        let url = site.url;
        
        if(title.length > 20) {
            title = title.substr(0, 20) + '...';
        }
        
        return {
            id, domain, title, fullTitle, favicon, url
        };
    }
    
    _getThumbnail(id) {
        let element = $(`[data-id=${id}]`);
        let domain = element.data('domain');
        
        $.get(`https://preview.whaleslide.com/get?url=http://${domain}`, (response) => {
            let thumbnail = response.preview ? response.preview : '/assets/images/icons/icon128.png';
            
            element.find('.WSP_pinned-pic').attr({
                'style':"background: url(" + thumbnail + ")  center/contain no-repeat"
            });
        });
    }
    
    _registerEvents() {
        let self = this;
        
        $(document).on('click', (e) => {
            if(e.target === this.$searchInput[0]) return;
            this._suggestionsHide();
        });
        
        $(document).on('click', ".WSP_remove", (e) => {
            let element = $(e.target);
            let link = element.parent().data('link');
            
            chrome.history.deleteUrl({
                url:link
            }, function() {
                element.parent().remove();
            })
        });
        
        $(document).on('click', ".WSP_search-suggestion", (e) => {
            let query = $(e.target).text();
            
            self.$searchInput.val(query);
            
            this.search();
        });
    }
}