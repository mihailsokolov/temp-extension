/**
 * INIT extension
 * First page that, preload scripts,
 */
class Extension {
    constructor() {
        this.init();
    }

    init() {
        /**
         * PostOffice - communication between background and content and popup scripts
         * @type {PostOffice}
         */
        this.postOffice = new PostOffice();
        this.extractDomain = new ExtractDomain();

        this.newTab = new NewTab(this);
    }

}

/**
 * Document ready
 */
$(document).ready(() => new Extension());
