class Organisations {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;
        this.apiService = rootScope.apiService;
        
        this.organisations = [];
        this.allOrganisations = [];
    
        this.listeners();
    }
    
    
    getAllOrganisations() {
        return new Promise(resolver => {
            if(!this.allOrganisations.length) {
                this.apiService.get('organisation/list?show_all=1').then(response => {
                    this.allOrganisations = response.organisations;
                    resolver(this.allOrganisations)
                });
            } else {
                resolver(this.allOrganisations)
            }
        })
    }
    
    getParentOrganisations() {
        return new Promise(resolver => {
            this.apiService.get('organisation/list?parent_id=' + Config.parent_organisation).then(response => {
                this.organisations = response.organisations;
                resolver(response.organisations);
            });
        })
    }
    
    
    init() {
        return new Promise((resolve) => {
            this.getAllOrganisations().then(() => this.getOrganisations()).then(() => resolve())
        });
    }
    
    getOrganisations() {
        return new Promise(resolver => {
            this.apiService.get('organisation/list?parent_id=' + Config.parent_organisation).then(response => {
                this.organisations = response.organisations;
                resolver(response.organisations);
            });
        })
    }
    
    
    
    getOrganisationById(id) {
//        console.log(id, this.organisations)
        return this.organisations.find(o => o.id == id);
    }
    
    
    getFromAllOrganisationsById(id) {
        return this.allOrganisations.find(o => o.id == id);
    }
    
    
    listeners() {
        this.postOffice.setEvent('organisations', r => {
            this.postOffice.postMessageRunTime(r.responseId, this.organisations)
        });
        
        this.postOffice.setEvent('getAllOrganisations', r => {
            this.getAllOrganisations().then(() => {
                this.postOffice.postMessageRunTime(r.responseId, this.allOrganisations)
            });
        });
        
        this.postOffice.setEvent('organisation', r => {
            this.rootScope.apiService.get(`organisation/${r.data}`).then(resp => {
                this.postOffice.postMessageRunTime(r.responseId, resp.organisation)
            });
        });
    }
    
}