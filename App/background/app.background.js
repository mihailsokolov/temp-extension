class App {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;
        this.apiService = rootScope.apiService;
        this.uiService = rootScope.uiService;
        this.organisations = rootScope.organisations;
    }
}