class Affiliates {
    constructor(rootScope) {
		this.rootScope = rootScope;
		this.postOffice = rootScope.postOffice;
		this.apiService = rootScope.apiService;
		this.uiService = rootScope.uiService;
		this.httpService = rootScope.httpService;

		this.listeners();
		
		this.checkLog = {};
    
        this.updateDomainsLog();
    }

    /**
     * Every targeted SERP in every tab gets their charity hints delivered by their own affiliates content script
     */
    listeners() {
        this.postOffice.setEvent('contentReady', () => {
            chrome.tabs.query({url: ["*://*/search*", "*://*/search/?text=*"]}, tab => tab.map(t => {
                
                
                    t.resultSelector =
                        /google/.test(t.url) ? '.r a' :
                            /yahoo/.test(t.url) ? '#main' :
                                /bing/.test(t.url) ? '#b_results' : '';

                    let org = this.rootScope.entity.entity;

                    t.resultSelector ? this.postOffice.postMessage({
						event: 'setCharityHints',
						data: [t.resultSelector, org.logo]
					}, t.id) : '';
                }
            ))
        });

		/**
		 * Affiliate check event
		 */
		this.postOffice.setEvent('affiliateCheck', response => {
		    
		    //Filter links that is not checked before
            const needToCheck = response.data.filter(l => !this.checkLog[l]);
            
            if(needToCheck.length) {
                let url = 'https://dev.affitise.com/api/v1/affiliate?urls[]=' + needToCheck.join('&urls[]=');
    
                this.httpService.post(url).then(checkedResponse => {
                    checkedResponse.forEach(r => {
                        this.checkLog[r.url] = {
                            url: r.url,
                            isAffiliate : r.isAffiliate,
                            date: new Date()
                        }
                    });
    
                    const affiliateResult = response.data.map(l => this.checkLog[l]);
                    this.postOffice.postMessageRunTime('affiliateCheck', affiliateResult.filter(r => r.isAffiliate));
                })
            }
            
            const affiliateResult = response.data.filter(l => !!this.checkLog[l]).map(l => this.checkLog[l]);
            this.postOffice.postMessageRunTime('affiliateCheck', affiliateResult.filter(r => r.isAffiliate));
            
            //Update log
            this.updateDomainsLog();
		});
	}
	
	async checkAffiliate(links) {
        //Filter links that is not checked before
        const needToCheck = links.filter(l => !this.checkLog[l]);
        
        if(needToCheck.length) {
            let url = 'https://dev.affitise.com/api/v1/affiliate?urls[]=' + needToCheck.join('&urls[]=');
            
            await this.httpService.post(url).then(checkedResponse => {
                checkedResponse.forEach(r => {
                    this.checkLog[r.url] = {
                        url: r.url,
                        isAffiliate : r.isAffiliate,
                        date: new Date()
                    }
                });
            })
        }
        
        return links.map(l => this.checkLog[l]);
    }
    
    //Clear log if link is longer then 24h;
    updateDomainsLog() {
        this.updateDomainsLogTimer ? clearTimeout(this.updateDomainsLogTimer) : '';
        this.updateDomainsLogTimer = setTimeout(() => {
            
            
            
            const dateNow = new Date();
            Object.keys(this.checkLog).forEach(key => {
                if(!this.checkLog[key]) {
                    return;
                }
                
                const timeDiff = Helpers.getTimeDiff(this.checkLog[key].date, dateNow);
                if(timeDiff > 1000 * 60 * 60 * 24) {
                    this.checkLog[key] = null;
                }
            });
            this.updateDomainsLog();
            
            
            
        }, 5000)
    }

}