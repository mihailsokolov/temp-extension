class Feedback {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;
        this.apiService = rootScope.apiService;
        this.uiService = rootScope.uiService;
        
        this.listeners();
    }
    
    //Set selected default site to local settings
    sendFeedback(r) {
        this.apiService.post(`feedback`, r.data).then(s => this.postOffice.postMessageRunTime(r.responseId, s));
    }
    
    openFeedback() {
        chrome.tabs.create({url:'App/singleton/index.html#feedback'});
    }
    
    listeners() {
        this.postOffice.setEvent('feedback', request => this.sendFeedback(request));
        this.postOffice.setEvent('openFeedback', request => this.openFeedback(request));
    }
    
}