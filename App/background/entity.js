class Entity extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.listeners();
    
        this.symbols = {
            CAD : 'C$',
            GBP : '£',
            USD : '$',
            EUR : '€'
        };
        
        this.entityChanges = new Emitter();
        
        this.entityChanges.subscribe(entity => {
            //Initially load icon to toolbar
            this.loadEntityIcon();
        });
    
        chrome.tabs.onActivated.addListener((activeInfo) => this.stream());
    }
    
    /**
     * Initial entity load.
     * Checking if UI have saved entity if so use it and save to storage
     * If not try to find in storage use it or take default parent organisation
     * @returns {Promise.<void>}
     */
    async init() {
        this.parentOrganisation = this.organisations.getFromAllOrganisationsById(Config.parent_organisation);
        
        //Last ui
        this.ui = this.uiService._ui.value;
        
        let fromUI;
        let notFound;
        if(this.ui.settings['extension'].entity) {
            fromUI = this.organisations.getOrganisationById(this.ui.settings['extension'].entity);
//            console.log(fromUI)
            notFound = !fromUI;
        }
        
        //If UI contains some entity use it first
        if(!!fromUI) {
            this.entity = fromUI;
            this.entity.selected = true;
        } else {
            
            //Check if storage have entity use it or take default
            await this.get().then(entity => {
                this.entity = entity || this.parentOrganisation;
                this.entity.selected = this.entity.id !== Config.parent_organisation;
                
                if(notFound) {
                    this.entity.notFound = true;
                }
            });
        }
    
        //Preload entity's charity domains
        await this.preloadEntityDomain();
        
        this.setSymbol();
    
        this.save();
    }
    
    setSymbol() {
        this.entity.symbol = this.symbols[this.entity['currency']];
    }
    
    /**
     * Save to storage and trigger stream
     */
    save() {
        chrome.storage.sync.set({entity:this.entity});
        this.stream();
    }
    
    /**
     * Preload entity's mysearch.co.uk domains
     */
    preloadEntityDomain() {
//        if(!this.ui.user) return;
        return this.apiService.get(`organisation/${this.entity.id}/domain`).then(r => this.entity.domains = r);
    }
    
    /**
     * Get from storage. use await
     * @returns {Promise}
     */
    get() {
        return new Promise(resolve => chrome.storage.sync.get('entity', s => resolve(s.entity)))
    }
    
    /**
     * Tell everyone that entity has been changed
     */
    stream() {
        this.entityChanges.emit(this.entity);
        this.postOffice.postMessageRunTime('_entity', this.entity);
    }
    
    /**
     * Change entity
     * If authorized save to user also
     */
    async changeEntity(r) {
        this.entity = r.data;
        //Selected true only if organisation is not default
        this.entity.selected = this.entity.id !== Config.parent_organisation;
        
        await this.saveToUser(r.data.id);
    
        //Preload entity's charity domains
        await this.preloadEntityDomain();
    
        //Response if have response id
        r.responseId ? this.postOffice.postMessageRunTime(r.responseId, r.data) : '';
    
        
        
        this.entityChanges.emit();
    
        this.setSymbol();
    
        this.save();
    }
    
    async reset() {
        await this.changeEntity({
            data: this.parentOrganisation
        });
    }
    
    /**
     * Call API to save entity to user
     * @param id
     * @returns {Promise}
     */
    saveToUser(id) {
        return new Promise(resolver => {
            if(this.uiService._ui.value['user']) {
                this.uiService.save('entity', id).then(r => resolver(r));
            } else {
                resolver(null)
            }
        })
    }
    
    /**
     * Entity can be found by code. This code check is on API side.
     * @param r
     */
    entityByCode(r) {
        this.apiService.get('organisation/list?parent_id=' + Config.parent_organisation + '&code=' + r.data, {}).then(response => {
            
            if(response.organisations && response.organisations.length) {
                
                r.data = response.organisations[0];
                
                this.changeEntity(r)
                
            } else {
                this.postOffice.postMessageRunTime(r.responseId, {
                    error:true, message:'No organisation found with this code!'
                });
            }
            
        }, error => {
            error.error = true;
            this.postOffice.postMessageRunTime(r.responseId, error);
        });
    }
    
    
    /**
     * Load entity's icon to the toolbar
     */
    loadEntityIcon() {
        if(!this.entity) return;
        
        this.timer ? clearTimeout(this.timer) : '';
        
        const canvas = document.createElement("canvas");
        const ctx = canvas.getContext("2d");
        
        const width = 128;
        const height = 128;
        
        canvas.width = width;
        canvas.height = height;
        
        const image = new Image;
        image.src = this.entity.logo;
        image.onload = () => {
            ctx.drawImage(image, 0, 0, width, height);
            
            chrome.browserAction.setIcon({
                imageData:ctx.getImageData(0, 0, width, height)
            });
        };
    }
    
    listeners() {
        this.postOffice.setEvent('setEntity', request => this.changeEntity(request));
        this.postOffice.setEvent('code', request => this.entityByCode(request));
        this.postOffice.setEvent('entity', r => this.postOffice.postMessageRunTime(r.responseId, this.entity));
        this.postOffice.setEvent('destroyEntity', r => this.postOffice.postMessageRunTime(r.responseId, this.entity));
        
        this.postOffice.stream('_entity').subscribe(request => this.stream());
    }
    
}