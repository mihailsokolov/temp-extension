class SplashOpenTrigger {
    
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;
        this.apiService = rootScope.apiService;
        this.uiService = rootScope.uiService;
        
        this.listeners();
    }
    
    open(target) {
        chrome.tabs.create({url:'App/splash/index.html#' + target});
    }
    
    listeners() {
        this.postOffice.setEvent('triggerSplash', (e) => this.open(e.data));
    }
}