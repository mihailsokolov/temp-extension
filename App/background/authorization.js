//                chrome.notifications.clear('accountCreated');
//                chrome.notifications.create('accountCreated', {
//                    type: "basic",
//                    title: "Welcome to WhaleSlide",
//                    message: "Your account was created successfully",
//                    iconUrl: "assets/images/icons/icon128.png"
//                });


class Authorization {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;
        this.apiService = rootScope.apiService;
        this.uiService = rootScope.uiService;
    
        this.authorizationInfo = null;

        this.init();
        
        this.authChange = new Emitter();
    }

    init() {
        this.listeners();
    }

    doLogin(r) {
        this.apiService.post('auth/login', r.data).then(response => {
            if (response.message === "token_generated") {
                this.onSuccess(r, response);
            }
        }, error => {
            error.error = true;
            this.postOffice.postMessageRunTime(r.responseId, error);
        });
    }


    /**
     * Sign up
     *  PayLoad:
     *
     *  confirmPassword:"qweqwe"
     *  password:"qweqwe"
     *  username:"qweqwe"
     */
    doSignUp(r) {
        this.apiService.post('auth/registration', r.data).then(async response => {
            if(response.message == "registration success") {
                
                await this.onSuccess(r, response);
                
                if(this.rootScope.entity.entity.selected) {
                    this.rootScope.entity.saveToUser(this.rootScope.entity.entity.id)
                }
            }
        },  error => {
            error.error = true;
            this.postOffice.postMessageRunTime(r.responseId, error);
        });
    }
    
    async onSuccess(request, response) {
        //Generated token saving to storage
        this.apiService.setToken(response.data.token);
    
        
        //Renew UI.
        await this.uiService.init().then(async () => {
    
            await this.rootScope.settings.changeSettings('guestMode', {data: false});
            await this.rootScope.entity.init();
            await this.rootScope.settings.init();
        
            //Return results to APP
            this.postOffice.postMessageRunTime(request.responseId, response);
            
            this.stream();
        });
    }
    
    
    
    async doLogout(r) {
        if(this.uiService._ui.value['user']) {
            this.apiService.removeToken();
    
            //Renew UI.
            await this.uiService.init();
            
            await this.rootScope.settings.changeSettings('guestMode', {data: true});
        } else {
            await this.rootScope.settings.changeSettings('guestMode', {data: false});
            await this.rootScope.entity.reset();
            await this.uiService.reset();
        }
    
        this.postOffice.postMessageRunTime(r.responseId, this.uiService._ui.value);
        this.stream();
    }

    /**
     *  Forgotten details:
     *  PayLoad:
     *
     *  email:"qweqwe@wefwe.ru"
     */
    doRecovery(r) {
        this.apiService.post('auth/password/reset', r.data).then(response => {
            this.postOffice.postMessageRunTime(r.responseId, response);
        },  error => {
            error.error = true;
            this.postOffice.postMessageRunTime(r.responseId, error);
        });
    }
    
    /**
     *  Link email
     *  PayLoad:
     *  {
            "email": "ksjfasdfas@fwewewe.ee"
        }
     */
    linkEmail(r) {
        this.apiService.put('user/email', r.data).then(response => {
            this.postOffice.postMessageRunTime(r.responseId, response);
        },  error => {
            error.error = true;
            this.postOffice.postMessageRunTime(r.responseId, error);
        });
    }
    
    /**
     * Tell everyone that entity has been changed
     */
    stream() {
        this.postOffice.postMessageRunTime('authorized', this.authorizationInfo);
        this.authChange.emit();
    }

    listeners() {
        this.postOffice.setEvent('login', request => this.doLogin(request));
        this.postOffice.setEvent('logout', request => this.doLogout(request));
        this.postOffice.setEvent('doSignUp', request => this.doSignUp(request));
        this.postOffice.setEvent('doRecovery', request => this.doRecovery(request));
        this.postOffice.setEvent('linkEmail', request => this.linkEmail(request));
    }

}

