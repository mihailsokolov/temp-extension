class DefaultSearchEngine {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;
        this.apiService = rootScope.apiService;
        this.uiService = rootScope.uiService;
        
        this.listeners();
        
        this.rootScope.uiService._ui.subscribe(ui => {
            if(ui.user) {
                this.init();
            } else {
                //Clear data when logout
                this.domains = this.defaults;
            }
        });
        
        this.defaults = [
            {
                domain_id: 'google',
                title: 'Google',
                url: 'https://www.google.lv/search?q='
            },
            {
                domain_id: 'bing',
                title: 'Bing',
                url: 'https://www.bing.com/search?q='
            },
            {
                domain_id: 'yahoo',
                title: 'Yahoo',
                url: 'https://search.yahoo.com/search?p='
            },
            {
                domain_id: 'yandex',
                title: 'Yandex',
                url: 'https://yandex.ru/search/?text='
            }
        ].map(d => {
            return {
                domain_id: d.domain_id,
                domain: {
                    organisation : d
                }
            }
        })
    }
    
    init() {
        //this.getUserDomainHistory();
    }
    
    
    getUserDomainHistory() {
        return new Promise(resolve => {
            this.apiService.get('user/domain/list').then(response => {
                if(response.user_domain_histories) {
                    this.domains = [...response.user_domain_histories, ...this.defaults];
                } else {
                    this.domains = [];
                }
                resolve(this.domains);
            })
        })
    }
    
    //Change default
    changeDefaultSearchEngine(r) {
        this.apiService.put('user/domain/' + r.data.domain_id).then(response => {
            this.getUserDomainHistory().then(domains => {
                this.postOffice.postMessageRunTime(r.responseId, domains)
            })
        })
    }

    //return to App domains. But first need to check if they exist.
    returnDomains(r) {
        if(this.domains) {
            this.postOffice.postMessageRunTime(r.responseId, this.domains)
        } else {
            this.getUserDomainHistory().then(() => {
                this.postOffice.postMessageRunTime(r.responseId, this.domains)
            })
        }
    }
    
    listeners() {
        this.postOffice.setEvent('getDomains', r => this.returnDomains(r));
        this.postOffice.setEvent('defaultSearchEngine', r => this.changeDefaultSearchEngine(r));
    }
    
}