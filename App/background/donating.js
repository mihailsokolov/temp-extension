/**
 * Donating controller
 */
class Donating {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;
        this.apiService = rootScope.apiService;
        this.uiService = rootScope.uiService;

        this.init();
    }

    init() {
        this.listeners();
    }
    
    changeDonating(r) {
        this.uiService.save('donating_everywhere', r.data).then((response) => {
            this.postOffice.postMessageRunTime(r.responseId, response);
        });
        
    }

    listeners() {
        this.postOffice.setEvent('changeDonatingOption', r => this.changeDonating(r));
    }

}