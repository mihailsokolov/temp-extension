class Supporting {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;
        this.apiService = rootScope.apiService;
        this.uiService = rootScope.uiService;
        
        this.listeners();
    }
    
    //Set selected default site to local settings
    setSupporting(r) {
        
        if(this.uiService._ui.value.user) {
            this.apiService.put(`organisation/${r.data.id}/support`).then(response => {
//                console.log(response)
            });
        }
        
        this.uiService.setLocalSettings('supporting', r.data);
        this.postOffice.postMessageRunTime(r.responseId, 'success');
    }
    
    listeners() {
        this.postOffice.setEvent('setSupporting', request => this.setSupporting(request));
    }
    
}