/**
 * This service check traffic and check if links are affiliates
 * If link is affiliate and it is not monetized yet send to the tab notification
 */

class Monetise {
    
    constructor(rootScope) {
        this.postOffice = rootScope.postOffice;
        this.rootScope = rootScope;
        this.http = rootScope.httpService;
        this.domainHelpers = new ExtractDomain();
        this.customHelpers = Helpers;
        
        
        //Clear log after auth
        this.rootScope.authorization.authChange.subscribe(() => this.clearLog());
        
        /**
         * Array to keep links that already was monetized
         * @type {Array}
         */
        this.affiliateLog = {};
        
        /**
         * Do not monitize this list of domains
         * @type {[*]}
         */
        this.deny = ['whaleslide', 'mysearch', 'google.', 'affitise.com'];
        
        /**
         * Keep all tabs with monetising
         * @type {Array}
         */
        this.windowToClose = [];
    
        //Detect monetising from WhaleSlide's pages
        //If this link was after affitise means that it was already monetised and do not show message again
        chrome.webRequest.onBeforeRequest.addListener(
            (details) => {
                const options = this.customHelpers.getQueryParams(details.url);
                if(!!options.ref && !!options.destination) {
//                    const extUser = this.rootScope.uiService._ui.value.user;
//                    if(!extUser) {
                        this.addToLog(this.domainHelpers.extractHostname(options.destination));
//                    } else {
//                        const split = options.ref.split('||');
//                        const userId = split[0];
//                        const supportingId = split[1];
//                    }
                    
                }
                
                return;
            },
            {urls: ["https://dev.affitise.com/*"]},
            []);
        
        
        /**
         * Decision when donation popup choose
         */
        this.postOffice.setEvent('donationDecision', (response, tab) => {
            //Extract domain from url
            const domain = this.domainHelpers.extractHostname(tab.url);
            
            if(response.data === 'yes' || response.data === 'always') {
                /**
                 * Run monetise this link
                 */
                this.doMonetise(domain, tab.tab.id);
            }
            
            if(response.data === 'always') {
                this.rootScope.settings.changeSettings('donating_everywhere', {data: 2});
            }
            
            if(response.data === 'no') {
                /**
                 * Save to log to now show popup again
                 */
                this.addToLog(domain);
            }
            
        });
        
        /**
         * If the id of a loaded tab matches the iterated one of the tabs array, then such tab receives the donation
         * popup. Moreover, if the extension is in tailor mode, then the custom logo is sent to be set in such popup
         */
        this.tabs = [];
        
        this.onTabReady();
        
        this.eventToCloseTempTab();
    }
    
    onTabReady() {
        chrome.tabs.onUpdated.addListener((tabId, info, additional) => {
            if(info.status == "complete") {
                
                //Extract domain from url
                const domain = this.domainHelpers.extractHostname(additional.url);
                
                //Stop if domain is blacklisted
                if(this.checkDeny(domain)) {
                    return;
                }
                
                //Find this domain in monetised log
                const logData = this.findInLog(domain);
                
                /**
                 * donating_everywhere
                 * 0 - do not donate
                 * 1 - ask every time
                 * 2 - always
                 */
                const donatingSettings = this.rootScope.settings.settings.donating_everywhere;
                
                
                if(!logData) {
                    this.rootScope.affiliates.checkAffiliate([domain]).then(response => {
                        if(response[0].isAffiliate) {
                            
                            if(donatingSettings == 1) {
                                chrome.windows.getCurrent({}, (w) => {
                                    if(w.type !== 'popup') {
                                        this.postOffice.postMessage({event:'donationPopup'}, tabId);
                                    }
                                });
                            }
                            
                            if(donatingSettings == 2) {
                                this.doMonetise(domain, tabId);
                            }
                        }
                    });
                }
                
                
            }
        });
    }
    
    /**
     * Check if tab is not blacklisted from deny array
     * @type {boolean}
     */
    checkDeny(domain) {
        return this.deny.find(item => domain.indexOf(item) !== -1);
    }
    
    findInLog(url) {
        return this.affiliateLog[url];
    }
    
    /**
     * Creates new tab with rediecting link
     * @param link
     * tabId - this is id to send notification about monetised status
     */
    doMonetise(domain, tabId) {
        const supporting = this.rootScope.entity.entity.id;
        const userId = this.rootScope.uiService._ui.value.user ? this.rootScope.uiService._ui.value.user.id : 0;
        const redirectTo = `https://dev.affitise.com/redirect?destination=${domain}&key=132128&ref=${userId}||${supporting}||app&platform=app`;
        
        /**
         * Create new pinned tab with redirect link
         */
        chrome.tabs.create({
            pinned:true, active:false, url:redirectTo
        }, (newTab) => {
            this.windowToClose.push({id:newTab.id, from:tabId});
        });
        
        this.addToLog(domain);
    }
    
    addToLog(domain) {
        this.affiliateLog[domain] = {
            url: domain,
            date: new Date()
        }
    }
    
    clearLog() {
        this.affiliateLog = {};
    }
    
    /**
     * Listen tab's updates and check if id of
     * this tab is in array windowToClose and close it
     */
    eventToCloseTempTab() {
        chrome.tabs.onUpdated.addListener((tabId, info) => {
            if(info.status == "complete") {
                //Close from queue
                this.windowToClose = this.windowToClose.filter(tab => {
                    if(tab.id === tabId) {
                        chrome.tabs.remove(tabId);
                        this.postOffice.postMessage({event:'monetiseReady'}, tab.from)
                    }
                    return tab.id !== tabId;
                });
            }
        });
    }
}