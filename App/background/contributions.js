class Contributions {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.postOffice = rootScope.postOffice;
        this.apiService = rootScope.apiService;
        this.uiService = rootScope.uiService;
        
        this.organisations = [];
        
        this.init();
        
        this.revenue = null;
        
        this.state = {
            busy: false
        };
    
        this.uiService._ui.subscribe(ui => {
            if(this.state.busy) {
                return;
            }
            
            if(ui['user']) {
                !this.revenue ? this.getRevenue() : '';
            } else {
                this.revenue = null;
            }
        })
    }
    
    getRevenue() {
        return new Promise(resolve => {
            this.state.busy = true;
            if(this.uiService._ui.value.user) {
                this.apiService.get('user/revenue?currency='+ this.rootScope.entity.entity['currency']).then(revenue => {
                    this.revenue = revenue;
                    this.state.busy = false;
        
                    resolve(this.revenue);
                })
            } else {
                resolve(this.revenue);
            }
            
        })
    }
    
    init() {
        this.listeners();
    }
    
    listeners() {
        this.postOffice.setEvent('changeContributionOption', r => {
            
            this.uiService.save('raised_by_you', r.data, 'supporting').then(() => {
                this.postOffice.postMessageRunTime(r.responseId, 'success');
            });
    
        });
        
        this.postOffice.setEvent('revenue', r => this.getRevenue().then(() => this.response(r)));
    }
    
    response (r) {
        this.postOffice.postMessageRunTime(r.responseId, this.revenue)
    }
    
}