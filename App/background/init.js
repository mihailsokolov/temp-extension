/**
 *
 *
 *
 *            ╔╗╔╗╔╗╔╗╔╗╔══╗╔╗──╔═══╗╔══╗╔╗──╔══╗╔══╗─╔═══╗
 *            ║║║║║║║║║║║╔╗║║║──║╔══╝║╔═╝║║──╚╗╔╝║╔╗╚╗║╔══╝
 *            ║║║║║║║╚╝║║╚╝║║║──║╚══╗║╚═╗║║───║║─║║╚╗║║╚══╗
 *            ║║║║║║║╔╗║║╔╗║║║──║╔══╝╚═╗║║║───║║─║║─║║║╔══╝
 *            ║╚╝╚╝║║║║║║║║║║╚═╗║╚══╗╔═╝║║╚═╗╔╝╚╗║╚═╝║║╚══╗
 *            ╚═╝╚═╝╚╝╚╝╚╝╚╝╚══╝╚═══╝╚══╝╚══╝╚══╝╚═══╝╚═══╝
 *
 *
 *                         =@@@@@@@@@@@@@@@=
 *                      %@@@@@@@@@@@@@@@@@@@@@@@@@%
 *                   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *                %@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%
 *              @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *             @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *          @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *         @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *       =@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@      =@@@@@@@@@@@@=
 *       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@           @@@@@@@@@@@
 *       @@@@@@@@@@@@ =@ %@@@@@@@@@@@@@@@@@             @@@@@@@@@@
 *       @@@@@@@@@@@@  =@@@@@@@@@@@@@@@@@@              @@@@@@@@@@
 *       @@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@              @@@@@@@@@@
 *       @@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@              @@@@@@@@@@
 *       @@@@@@@@@@@@  @@@@@@@@@@@@@@@@@@              @@@@@@@@@@@
 *       @@@@@@@@@@@@=  @@@@@@@@@@@@@@@@              @@@@@@@@@@@@
 *       =@@@@@@@@@@@@   =@@@@@@@@@@@@               @@@@@@@@@@@@=
 *        @@@@@@@@@@@@@@                            @@@@@@@@@@@@@
 *         @@@@@@@@@@@@@@%                        @@@@@@@@@@@@@@
 *          @@@@@@@@@@@@@@@@                   @@@@@@@@@@@@@@@@
 *           @@@@@@@@@@@@@@@@@@@@         @@@@@@@@@@@@@@@@@@@@
 *             @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *              @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *                %@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%
 *                   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *                      %@@@@@@@@@@@@@@@@@@@@@@@@@%
 *                           =@@@@@@@@@@@@@@@=
 *
 *
 *
 *
 */
class Extension {
	constructor() {

		this.onInstall();
		
		this.init();
	}
    
    
    /**
     * Loading steps:
     * 1. Load all organisations
     * 2. Load organisations from parent ID
     * 3. Load token from storage (async)
     * 4. Load UI
     * 5. Entity
     * 6. Settings
     */
	async init() {
	    //Ready state
        this.ready = new Emitter();
        
		/**
		 * Post office
		 * Make conversation between background, content script and popup script
		 * @type {PostOffice}
		 */
		this.postOffice = new PostOffice( true );
        this.httpService = new HttpService( this );


		//Api service
		this.apiService = new ApiService( this );
        //Organisations component
        this.organisations = new Organisations( this );
        //UI service init
        this.uiService = new UiService( this );
        //Entity component
        this.entity = new Entity( this );
        //Settings component
        this.settings = new SettingsBackground( this );
        //Authorizations component
        this.authorization = new Authorization( this );
    
        /**
         * Let's Go!
         */

        //1. All organisations
        await this.organisations.getAllOrganisations();
        //2. Parent organisations
        await this.organisations.getParentOrganisations();
		//3. Token from storage
        await this.apiService.loadToken();
        //4. UI service init
        await this.uiService.init();
        //5. Entity component
        await this.entity.init();
        //6. Settings component run
        await this.settings.init();
    
        
        
        new Contributions( this );
        new SplashOpenTrigger( this );
        
        //Affiliates
        this.affiliates = new Affiliates( this );
        new Monetise( this );
        new Feedback( this );
    
        this.ready.emit(true);
		
//		this.apiService.loadToken().then(() => {
//
//
//            this.uiService = new UiService( this );
//
//            this.httpService = new HttpService( this );
//
//            this.uiService.init().then(() => {
//                //Login, logout, signup, guest mode, recovery,  link email
//                this.authorization = new Authorization( this );
//
//                //Get organisations list from API
//                this.organisations = new Organisations( this );
//                this.organisations.init().then(() => {
//                    this.entity = new Entity( this );
//                    new Preloader(this);
//                });
//
//
//                //Events to trigger splash screen
//                new SplashOpenTrigger( this );
//                new Donating( this );
//                new Supporting( this );
//                new Contributions( this );
//
//                new Affiliates( this );
//                new Monetise( this );
//
//
//                this.sockets = new SocketService( this );
//                //this.defaultSites = new DefaultSearchEngine( this );
//            })
//
//
//        })

	}

	/**
	 * Upon installation the browser navigates to the url set in config.js, upon update a log states the version change
	 */
	onInstall() {
		chrome.runtime.onInstalled.addListener( ( details ) => {
			if ( details.reason == "install" && Config.open_splash_on_install ) {
                chrome.tabs.create( {
                    active: true,
                    url: chrome.extension.getURL( 'App/splash/index.html' )
                } );
                
//                uiSubs.unsubscribe();
			}

//            details.reason == "install" && config.thank_you_url ?
//                chrome.tabs.create({
//                    active: true,
//                    // url: config.thank_you_url
//                }) : '';

			details.reason == "update" ? console.log( `Updated from ${details.previousVersion} to ${chrome.runtime.getManifest().version}!` ) : '';
		} );
	}
}
new Extension();