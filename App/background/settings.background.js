class SettingsBackground extends App {
    constructor(rootScope) {
        super(rootScope);
        
        this.listeners();
        
        this.settingsChanges = new Emitter();

        //Await until ready
        this.readyState = new Promise(resolver => this.readyStateResolver = resolver);
        
        this.settingsChanges.subscribe(settings => {
            this.readyStateResolver();
        });
    }
    
    /**
     * Initial entity load.
     * Checking if UI have saved entity if so use it and save to storage
     * If not try to find in storage use it or take default parent organisation
     * @returns {Promise.<void>}
     */
    async init() {
        this.ui = this.rootScope.uiService._ui.value;
    
        //Default settings. taken from ui
        this.settings = {
            guestMode: false,
            donating_everywhere: this.ui.settings['extension'].donating_everywhere,
            raised_by_you: this.ui.settings['supporting'].raised_by_you
        };
        
        if(!this.ui['user']) {
            let settings;
            await this.get().then(s => settings = s);
            if(settings) {
                this.settings = settings;
            }
        }
        
        this.save();
    }
    
    /**
     * Save to storage and trigger stream
     */
    save() {
        chrome.storage.sync.set({settings:this.settings});
        this.stream();
    }
    
    /**
     * Get from storage. use await
     * @returns {Promise}
     */
    get() {
        return new Promise(resolve => chrome.storage.sync.get('settings', s => resolve(s.settings)))
    }
    
    /**
     * Tell everyone that entity has been changed
     */
    stream() {
        this.settingsChanges.emit(this.settings);
        this.postOffice.postMessageRunTime('_settings', this.settings);
    }
    
    /**
     * Change entity
     * If authorized save to user also
     */
    async changeSettings(key, r) {
        this.settings[key] = r.data;
        
        this.save();
        
        await this.saveToUser(key, r.data);
        
        this.postOffice.postMessageRunTime(r.responseId, r.data);
        
        this.settingsChanges.emit();
    }
    
    /**
     * Call API to save entity to user
     * @param id
     * @returns {Promise}
     */
    saveToUser(key, id) {
        return new Promise(resolver => {
            if(this.uiService._ui.value.user) {
                this.uiService.save(key, id).then(r => resolver(r));
            } else {
                resolver(null)
            }
        })
    }
    
    listeners() {
        this.postOffice.setEvent('settings', async r => {
            
            if(!this.settings) {
                await this.readyState;
            }
            this.postOffice.postMessageRunTime(r.responseId, this.settings)
        });
        this.postOffice.stream('_settings').subscribe(request => this.stream());
        
        this.postOffice.setEvent('settingsDonatingOption', r => this.changeSettings('donating_everywhere', r));
        this.postOffice.setEvent('settingsContributionOption', r => this.changeSettings('raised_by_you', r));
        this.postOffice.setEvent('guestMode', async r => {
            await this.changeSettings('guestMode', r);
            this.rootScope.authorization.stream();
        });
    }
    
}