/**
 * INIT extension
 * First page that, preload scripts,
 */
class Extension {
    constructor() {
        this.$$wrapper = $('.WSP_wrapper');
        /**
         * PostOffice - communication between background and content and popup scripts
         * @type {PostOffice}
         */
        this.postOffice = new PostOffice();
        
        this.run();
    }
    
    async run() {
        this.router = new Router(this);
    
        //Global error component
        this.errors = new Errors(this, {
            parent: this.$$wrapper,
            position: 'top'
        });
    
        this.init();
    }
    
   
    init() {

    }
}

/**
 * Document ready
 */
$(document).ready(() => new Extension());
