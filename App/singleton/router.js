class Router extends App {
    
    constructor(rootScope) {
        super(rootScope);
        
        this.current = null;
        this.reveal = null;
    
        this.hash = window.location.hash.replace("#", '');
    
        if(this.hash && this.hash.length) {
            this.navigate(this.hash);
        }
        
    }
    
    navigate(page, options = null) {
        switch (page) {
 
            case 'feedback':
                return this.reveal = new FeedbackSingleton(this.rootScope);
                break;
            default:
                console.error('No such routes');
        }
    }
}