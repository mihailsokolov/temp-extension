class FeedbackSingleton extends App  {
    constructor(rootScope) {
        super(rootScope);
        
        
        this.show();
    }
    
    show() {
        this.elements();
        this.setEvents();
    }
    
    
    elements() {
        this.$container = $(this.content());
    
        //Submit button
        this.submitBtn = new SubmitBtn({
            parent: this.find('ui-submit'),
            title:'Submit'
        });
        
        //errors
        this.error = new Errors(this.rootScope, {
            parent: this.find('.js-error-container'),
            position: 'top'
        });
    
        this.reveal = new Reveal(this.rootScope, {
            content: this.$container,
            title:'What’s your idea?'
        });
        
        this.$form = {
            email: this.find('.js-email'),
            subject: this.find('.js-subject'),
            description: this.find('.js-description'),
        }
    }
    
    setEvents() {
        this.reveal._on('closed', () => {
            if(!confirm('Are you sure you want to exit? This action will close this tab')) {
                this.reveal.preventDefault();
            } else {
                window.close()
            }
        });
    
        /**
         *
         *
         * feedback
         from : "elfewf@fewfewf.rr"
         message : "wevvwevwev"
         subject : "wefwefwef"
         title : "Tell us your idea!"
         */

        this.$container.on('submit', (e) => {
            e.preventDefault();
    
            this.removeRedLines();
            
            if(this.$form.email.val().length < 3) {
                this.$form.email.addClass('WSP_error-input');
                this.error.show('Email is empty');
                return;
            }
            
            if(this.$form.subject.val().length < 3) {
                this.$form.subject.addClass('WSP_error-input');
                this.error.show('Subject is empty');
                return;
            }
            
            if(this.$form.description.val().length < 3) {
                this.$form.description.addClass('WSP_error-input');
                this.error.show('Description is empty');
                return;
            }
    
            this.submitBtn.state('busy');
            
            const payload = {
                from: this.$form.email.val(),
                message: this.$form.description.val(),
                subject: this.$form.subject.val(),
                title: 'Extension Feedback'
            };
            
            this.postOffice.post('feedback', payload).then(() => {
                this.submitBtn.state('success');
                
                this.clearFilds();
            })
        });
    
        //On keydown destroy error message
        $.each(this.$form, (index, field) =>  field.on('keydown', () => {
            this.error.destroy();
            this.removeRedLines();
        }));
        
        this.find('.js-close').on('click', e => {
            e.preventDefault();
            
            this.reveal.destroy();
        })
    }
    
    clearFilds() {
        $.each(this.$form, (index, field) =>  field.val(''));
    }
    
    removeRedLines() {
        this.find('.WSP_error-input').removeClass('WSP_error-input');
    }
    
    destroy() {
        this.$container.remove();
        this.rootScope.errors.destroy();
    }
    
    content() {
        return `
        <form class="WSP_feedback js-feedback" style="width: 480px; overflow: hidden">
        
            <div class="js-error-container" style="position: relative"></div>
            
            <div style="padding: 0 20px;">
                <div class="upset small"></div>
                
                <ul class="WSP_list upset small">
                    <li class="WSP_cursor-pointer">
                        <input type="text"
                           name="email"
                           value=""
                           tabindex="1"
                           placeholder="Your email"
                           class="WSP_main-input WSP_inline WSP_main-input js-email">
                    </li>
                    <li class="WSP_cursor-pointer">
                        <input type="text"
                           name="subject"
                           value=""
                           tabindex="2"
                           placeholder="Subject"
                           class="WSP_main-input WSP_inline WSP_main-input js-subject">
                    </li>
                    
                    <li> 
                        <textarea class="WSP_main-input js-description" placeholder="Give a brief description"></textarea>
                    </li>
                </ul>
                
                <div class="WSP_clearfix downset small">
                    <div class="WSP_float-right">
                        <ui-submit></ui-submit>
                    </div>
                    <div class="WSP_float-left">
                        <button class="WSP_button-main WSP_smaller WSP_grey js-close">close</button>
                    </div>
                </div>
            </div>
        
        </form>
        `
    }
}