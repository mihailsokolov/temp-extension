//const DEV = true;
const DEV = false;

const __PATH = DEV ? 'apidev.whaleslide.com' : 'api.whaleslide.com';

const Config = {
    "api_url": "https://" + __PATH + "/api/v1/",
    "default_search": false,
//    "supporting_options_visible_always" : true,//true for WebGive
    "code_picker" : true,//true for WebGive
    "defaultSites_continue_with_default" : false, //false for WebGive,
    "parent_organisation" : DEV ? 214 : 83, //214 DEV
    "open_splash_on_install": true, //true for WebGive
    "name": 'WebGiv',
    
    api: {
        public: {
            protocol: 'https',
            host:   'search.whaleslide.com',
            path:     '/api',
            version:  'v1',
            token: 'f8OhUDEYKUICHDnIxEgI7Cb4uYyTBqT4nO8iueNbfTO3devS24yElGqM7nCm',
            settings: {
                num:            20,
                suggestionsNum: 5,
            }
        },
        private: {
            protocol: 'https',
            host:   __PATH,
            path:     '/api',
            version:  'v1',
        },
    }
    
};