class Emitter {
    constructor() {
        this._callbacks = [];
        this.value;
    }
    
    subscribe (callback) {
        const _id = this.uuidv4();
        const event = {
            _id: _id,
            callback : callback,
            unsubscribe: () => this._callbacks = this._callbacks.filter(call => call._id !== _id)
        };
        
        this._callbacks.push(event);
        
        return event;
    }
    
    emit(value) {
        this.value = value;
        this._callbacks.forEach(call => call.callback(value))
    }
    
    //Create random id for event
    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}