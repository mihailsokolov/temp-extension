class SocketService {
    constructor(rootScope) {
        this.rootScope = rootScope;
    
        this.url = Config.api['private'].protocol + '://' + Config.api['private'].host;
        
        this.run();
    }
    
    run() {
        this.rootScope.uiService._ui.subscribe(ui => {
            
            if(ui.user && !this.socket) {
                this.socket = io(this.url, {
                    path: '/pusher',
                    query: 'user_id=' + ui.user.id,
                    secure: true
                });
            
            
            
                this.socket.on('user-' + ui.user.id, (data) => {
                    console.log(JSON.parse(data));
                    let message = JSON.parse(data)['update'];
                
                
                
                    switch(message.type) {
                        case 'url_notification':
                            console.log("trigger")
                            chrome.tabs.create({
                                active: true,
                                url: message.url
                            });
                            break;
                    }
                
                });
            }
        
        });
    }
}