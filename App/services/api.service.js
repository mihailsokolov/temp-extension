class ApiService {
    constructor(rootScope) {
        this.rootScope = rootScope;
        // this.ui = rootScope.ui;
        
        this.customOrigin = null;
        
        this.rootScope.ready.subscribe(() => {
            if(this.rootScope.entity.entity.domains) {
                this.customOrigin = this.rootScope.entity.entity.domains.domains[0].domain;
            }
            
            this.rootScope.entity.entityChanges.subscribe(e => {
                this.customOrigin = e && e.domains ? e.domains.domains[0].domain : null;
            });
        });

        this.api_url = Config.api_url;
    }

    get(link, data = {}) {
        return this.request('GET', link, data)
    }

    post(link, data = {}) {
        return this.request('POST', link, data)
    }

    put(link, data = {}) {
        return this.request('PUT', link, data)
    }

    delete(link, data = {}) {
        return this.request('DELETE', link, data)
    }

    request(method, link, data) {
        return new Promise((resolve, reject) => $.ajax({
            type: method,
            url: this.api_url + link,
            data: data,
            headers: {
                "Authorization": this.token,
                "X-CUSTOM-ORIGIN" : this.mapOrigin(this.customOrigin)
            }
        }).done(
            data => resolve(data)
        ).error(
            error => reject(JSON.parse(error.responseText))
        ));
    }
    
    mapOrigin(o) {
        if(!o) {
            return o;
        }
        return o.indexOf('http://') === -1 || o.indexOf('https://') === -1 ? 'http://' + o : o;
    }
    
    loadToken() {
        return new Promise(resolve => {
            chrome.storage.sync.get(['token'], r => {
                if(r.token) {
                    this.setToken(r.token);
                }
    
                resolve();
            });
        })
    }

    setToken(token) {
        this.token = 'Bearer ' + token;
        chrome.storage.sync.set({token: token});
    }

    removeToken() {
        chrome.storage.sync.remove('token');
        this.token = ''
    }

    removeStorage() {
        chrome.storage.sync.remove('ui');
        this.removeToken();
        this.token = null;
    }
}