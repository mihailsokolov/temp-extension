class Loader {
    constructor() {

    }

    beforeApp() {
        return new Promise(resolver => {
            this.loadScript([
                '/App/popup/app.js',
                '/App/config.js',
                '/App/services/postOffice.js',
                '/App/ui/snippets/list.js',
                '/App/ui/ui-login.js',
                '/App/ui/header.js',
                '/App/ui/error/errors.js',
                '/App/ui/logo/logo.js',
            ]).then(res => resolver())
        })
    }

    notAuthorized() {
        return new Promise(resolver => {
            if (typeof NotAuthorized === 'undefined') {
                this.loadScript([
                    '/App/popup/components/not-authorized/notAuthorized.js'
                ]).then(() => resolver())
            } else {
                resolver()
            }
        })
    }

    supportingList() {
        return new Promise(resolver => {
            if (typeof SupportingList === 'undefined') {
                this.loadScript([
                    '/App/popup/components/guest/supporting.js',
                ]).then(() => resolver())
            } else resolver();
        })
    }

    login() {
        return new Promise(resolver => {
            if (typeof LogIn === 'undefined') {
                this.loadScript([
                    '/App/popup/components/login/login.js'
                ]).then(() => resolver())
            } else {
                resolver()
            }
        })
    }

    help() {
        return new Promise(resolver => {
            if (typeof HelpPage === 'undefined') {
                this.loadScript([
                    '/App/popup/components/help/help.js',
                    '/App/ui/help/help.js',

                ]).then(() => resolver())
            } else {
                resolver()
            }
        })
    }


    index() {
        return new Promise(resolver => {
            if (typeof Index === 'undefined') {
                this.loadScript([
                    '/App/popup/components/contribution/total.js',
                    '/App/popup/components/contribution/switcher.js',
                    '/App/popup/components/index.js',
                ]).then(() => resolver())
            } else {
                resolver()
            }
        })
    }

    settings() {
        return new Promise(resolver => {
            if ( typeof Settings === 'undefined' ) {
                this.loadScript([
                    '/App/popup/components/authorized/settings.js'
                ]).then(() => resolver())
            } else {
                resolver()
            }
        })
    }
    
    
    settingsDonating() {
        return new Promise(resolver => {
            if ( typeof SettingsDonating === 'undefined' ) {
                this.loadScript([
                    '/App/popup/components/authorized/donating-settings-page.js'
                ]).then(() => resolver())
            } else {
                resolver()
            }
        })
    }

    
    
    codePicker() {
        return new Promise(resolver => {
            if ( typeof CodePicker === 'undefined' ) {
                this.loadScript([
                    '/App/ui/ui-codePicker.js',
                    '/App/popup/components/guest/codePicker.js'
                ]).then(() => resolver())
            } else {
                resolver()
            }
        })
    }

    defaultSearchEngine() {
        return new Promise(resolver => {
            if ( typeof DefaultSearchEngine === 'undefined' ) {
                this.loadScript([
                    '/App/popup/components/authorized/default-search.js'
                ]).then(() => resolver())
			} else {
                resolver();
			}
		})
    }

    addCustomEngine() {
        return new Promise(resolver => {
            if ( typeof AddCustomEngine === 'undefined' ) {
                this.loadScript([
                    '/App/popup/components/authorized/add-custom-engine.js'
                ]).then(() => resolver())
            } else {
                resolver();
            }
        })
    }
    
    
    
    defaultSites() {
        return new Promise(resolver => {
            if ( typeof DefaultSites === 'undefined' ) {
                this.loadScript([
                    '/App/popup/components/guest/defaultSites.js'
                ]).then(() => resolver())
            } else {
                resolver();
            }
        })
    }


    loadScript(urls) {
        let loadedScripts = 0;

        return new Promise(resolve => {
            const callback = () => {
                if (++loadedScripts === urls.length) {
                    resolve();
                }
            };


            urls.forEach(url => {
                // Adding the script tag to the head as suggested before
                const head = document.getElementsByTagName('head')[0];
                const script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = url;

                // Then bind the event to the callback function.
                // There are several events for cross browser compatibility.
                script.onreadystatechange = callback;
                script.onload = callback;

                // Fire the loading
                head.appendChild(script);
            })
        })
    }
}