class UiService {
    constructor(rootScope) {
        this.rootScope = rootScope;
        this.apiService = rootScope.apiService;
        this.postOffice = rootScope.postOffice;
        
        
        this.listeners();
    
        this._ui = new Emitter();
        
        //Await until ready
        this.readyState = new Promise(resolver => this.readyStateResolver = resolver);
    
        this._ui.subscribe(ui => {
            this.stream();
            this.readyStateResolver();
        });
    }
    
    /**
     * Distributes ui. Can use as renew ui
     * @return {Promise}
     */
    init() {
        return new Promise(async resolve => {
            await this.getUi().then((ui) => {
                if(!ui.user) {
                    //If ui is not authorized getting UI from storage
                    this.getUiStorage().then(ui => {
                        this._ui.emit(ui);
                        resolve(this._ui.value);
                    });
                } else {
                    //If UI is authorized just set it to memory
                    this._ui.emit(ui);
                    resolve(this._ui.value);
                }
            });
        });
    }
    
    /**
     * GET UI from server
     */
    getUi() {
        return new Promise(resolve => this.apiService.get('ui').then(ui => resolve(ui)))
    }
    
    async reset() {
        await this.getUi().then(ui => this._ui.emit(ui));
        this.setToStorage({ui:this._ui.value});
        
        this.stream();
    }
    
    /**
     * Get UI from storage. If storage is empty request new UI from server and set to storage
     * @returns {Promise}
     */
    getUiStorage() {
        return new Promise(resolve => {
            chrome.storage.sync.get('ui', storage => {
                if(storage.ui) {
                    resolve(storage.ui)
                } else {
                    this.getUi().then((ui) => {
                        this.setToStorage({ui:ui});
                        resolve(ui);
                    })
                }
            })
        })
    }
    
    setLocalSettings(target, option) {
        this._ui.value.extension[target] = option;
        this._ui.emit(this._ui.value);
        this.stream();
    }
    
    save(key, option, setting = 'extension') {
        return new Promise(resolve => {
            /**
             * Send data to endpoint
             * Resolve promise on success
             */
            
            this._ui.value.settings[setting][key] = option;
            
            
            if(this._ui.value.user) {
                this.apiService.put('setting', {[key]:this._ui.value.settings[setting][key]}).then(result => resolve());
            } else {
                this.setToStorage({ui:this._ui.value});
                
                resolve();
            }
        }).then(() => {
            
            this.stream()
        })
    }
    
    /**
     * Setting data to chrome storage
     * @param data
     */
    setToStorage(data) {
        chrome.storage.sync.set(data)
    }
    
    test() {
        setTimeout(() => {
            this.postOffice.postMessageRunTime('_ui', this._ui.value);
            this.test();
        }, 1000)
    }
    
    stream() {
        this._ui.value ? this.postOffice.postMessageRunTime('_ui', this._ui.value) : '';
    }
    
    /**
     * Listeners from other extension's sides
     */
    listeners() {
        this.postOffice.setEvent('ui', async request => {
            if(!this._ui.value) {
                await this.readyState;
            }
            this.postOffice.postMessageRunTime(request.responseId, this._ui.value)
        });
        this.postOffice.stream('_ui').subscribe(request => this.stream());
    }
}