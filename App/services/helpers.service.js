const Helpers = {
    getUrl: function (key, args) {

        let domain = WhaleSlide_config.WSurl + WhaleSlide_config.actionUrls[key];

        if(args) {
            for (let arg in args) {
                domain = domain.replace("\$\{"+ arg +"\}", args[arg]);
            }
        }

        return domain;
    },

    sanitLink : function (link, args = false) {
        if(args) {
            for (let arg in args) {
                link = link.replace("\$\{"+ arg +"\}", args[arg]);
            }
        }

        return link;
    },

    getQueryParams : function(qs) {
        qs = qs.split("?");
        gs = typeof qs[1] == 'undefined' ? qs[0] : qs[1];
        qs = gs.split('+').join(' ');

        let params = {},
            tokens,
            re = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = re.exec(qs)) {
            params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
        }

        return params;
    },

    getTimeDiff : function(d1, d2) {
        let diff =  Math.abs(new Date(d2) - new Date(d1));

        let seconds = Math.floor(diff/1000); //ignore any left over units smaller than a second
        // var minutes = Math.floor(seconds/60);
        //     seconds = seconds % 60;
        // var hours = Math.floor(minutes/60);
        //     minutes = minutes % 60;
        return  seconds;
    }
}