/**
 * Sends an AJAX request to validate if the link is affiliated
 */

class HttpService {

    constructor(rootScope) {
        this.bearer = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjMxMjZlNjdmMzlhN2E3ODI0NTc0OWZlYjFjOWVmYWUyNmY1MjU0Y2Q5ZTE4Y2NhMTBmODA0NmI1MjE3Y2I0ZTcyZWQ2MzRiZWIxNWNlODVlIn0.eyJhdWQiOiIxIiwianRpIjoiMzEyNmU2N2YzOWE3YTc4MjQ1NzQ5ZmViMWM5ZWZhZTI2ZjUyNTRjZDllMThjY2ExMGY4MDQ2YjUyMTdjYjRlNzJlZDYzNGJlYjE1Y2U4NWUiLCJpYXQiOjE0OTU0OTE2NTEsIm5iZiI6MTQ5NTQ5MTY1MSwiZXhwIjoxNTI3MDI3NjUxLCJzdWIiOiIyIiwic2NvcGVzIjpbImNoZWNrLWlmLWFmZmlsaWF0ZSJdfQ.Z-vNm7gLfi4AFaJesDVnueYiv10PPoFPokN2nMIrQiibt2dwE_7SMgNCvmiLMQi9xgMgPo2bcNICZbi4pRVwP8yPNFHGC7KqrcQ64_sM-GygxHuB4vjWeiaOkgcJXjDBS_Ekp__W_h3Pa4NW7fHXtqfNjAcs8aW4VoYeb0gEbpL612eKfI2rgdnA58wwUXWDmZKtSzlyD_cLHDfBSt1U6gX0SmFknoPl4qQAKhVr0LQPFWTBmn_o4WrlrjVm_Urq_aVu_uibQcronqLh9LZoKJGqhGBJGtiA9sGvkvrzx-NgsqLXzsvTG_VsR864z5V0ObsbQUxKPIhUZrOHQXUv1iqNC2s3QRLCwTwg08cFjJHkRPWu6rcY8-GpctMvQCobcqxUYQMMGLPv6e5DDuoC9ZDsZSlLpANi1LN2eeQL6V5eG2FsHMU60Azmd0OHLrPJ2kUa7qsuPTpNZgGBPjL-XbMrxYu4NtZ6qhpXPTn9N_gjHDsZjtAeWd3Jpl5GMuz_OGZWbXrmR6S0R03Q0PIO1svfLItBPD6RBpIg78CPub0RuItZAOjxd1hcrgVWP4PLHuB4U6EgQP2whNzYHdd9XbrWFGAJccmJOms7cGAKmCOJJWYWLbfo2_zZruV_c_qKLEfMLF6ybdsPRMLTjQL-7e64ltS82ezB5jd4RYnYGOs'
        this.token = null;
    }

    get(link) {
        let resolver = (resolve) => {
            $.ajax({
                type: 'GET',
                url: link,
                headers: {
                    "Authorization": this.bearer
                }
            }).done((data) => resolve(data));
        };
        return new Promise(resolver);
    }

    post(link, data = {}) {
        let resolver = (resolve) => {
            $.ajax({
                type: 'POST',
                url: link,
                data : data,
                headers: {
                    "Authorization": this.bearer
                }
            })
                .done((data) => {
                    resolve(data)
                })
                .error(error => {
                    if(this.isJson(error.responseText)) {
                        resolve(JSON.parse(error.responseText))
                    }
            });
        };
        return new Promise(resolver);
    }

    isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
}

