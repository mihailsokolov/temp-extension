class PostOffice {
    constructor(background = false) {
        this.runEvent();
        
        this.eventOnce = [];
        this.background = background;
        
        this.streamEvents = {};
        this.requested = {};
        this.streamLastResponse = {};
        this.events = [];
    }
    
    runEvent() {
        chrome.runtime.onMessage.addListener((response, tab, callback) => {
            
            //For background we need to get first part of request id, but for others get full id
            const event = this.background && response.responseId
                ? response.responseId.split('-')[0]
                : response.event;
            
            if(!event) {
                return;
            }

            //Keep last response for every event. using for stream
            this.streamLastResponse[event.split('-')[0].split('_')[1]] = response;
            
            if(this.streamEvents[event]) {
                this.streamEvents[event].forEach(async e => e.callback(response));
                this.requested[event] = false;
            }
            
            const registered = this.events.find(e => e.id === event);

            if(!registered) {
                return;
            }

			registered.callback(response, tab);
        });
    }
    stream(e) {
        return {
            subscribe:(callback) => {
                const _id = this.uuidv4();
                //event payLoad
                const event = {
                    id:_id,
                    
                    callback:response => {
                        callback(response.data);
                    },
                    
                    unsubscribe:() => {
                        this.streamEvents[e] = this.streamEvents[e].filter(call => call.id !== _id)
                    }
                };
                
                
                if(!this.streamEvents[e]) {
                    this.streamEvents[e] = []
                }
                this.streamEvents[e].push(event);
                
                const _e = e.split('_')[1];
                if(this.streamLastResponse[_e]) {
                    callback(this.streamLastResponse[_e].data)
                } else {
                    this.postMessageRunTime(e, {});
                }
                
                return event;
            },
            
        }
    }
    
    /**
     * Register events
     * @param event
     * @param callback
     */
    setEvent(event, callback) {
        //Find if not exist
        if(this.events.find(e => e.id === event)) {
            throw `Event '${event}' already registered`
        }
        
        //add events to the list
        this.events.push({
            id:event, callback:callback
        });
        
        return {
            unsubscribe: () => {
                this.events = this.events.filter(e => e.id !== event);
            }
        }
    }
    
    
    /**
     * One time response. USE IT ONLY FOR CONTENT AND POPUP SCRIPTS
     * @param event
     * @returns {Promise}
     */
    get(event) {
        return this.request(event, {});
    }
    
    post(event, data) {
        return this.request(event, data);
    }
    
    request(event, data) {
        return new Promise((resolve, reject) => {
            const eventId = event + '-' + this.uuidv4();
            this.setEvent(eventId, (response, tab) => {
                
                //Response. Check if error exist -> response to reject
                !response.data || !(response.data && response.data.error) ? resolve(response.data) : reject(response.data);
                //Remove event from register
                this.events = this.events.filter(e => e.id !== eventId);
            });
            
            this.postMessageRunTime(event, data, eventId);
        })
    }
    
    postMessage(message, tabId = null) {
        if(tabId) {
            chrome.tabs.sendMessage(tabId, message);
        } else {
            chrome.tabs.query({active:true, currentWindow:true}, (tabs) => {
                if(tabs[0]) {
                    chrome.tabs.sendMessage(tabs[0].id, message);
                }
            });
        }
    }
    
    
    /**
     * Runtime response. Send data from background to content script
     * @param event
     * @param data
     * @param responseId
     */
    postMessageRunTime(event, data, responseId) {
        const message = { event:event, responseId:responseId, data:data };
        chrome.runtime.sendMessage(message);
    
        if(this.background) {
            this.postMessage(message)
        }
    }
    
    //Create random id for event
    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}